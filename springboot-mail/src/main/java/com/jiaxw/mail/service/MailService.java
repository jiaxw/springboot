package com.jiaxw.mail.service;

import org.thymeleaf.context.Context;

/**
 * 邮件
 *
 * @author jiaxw
 * @date 2020/12/1 20:04
 */
public interface MailService {

    /**
     * 发送简单邮件
     *
     * @param to      发给谁
     * @param subject 标题
     * @param content 邮件内容
     */
    void sendSimpleMail(String to, String subject, String content);

    /**
     * 发送 html 邮件
     *
     * @param to      发给谁
     * @param subject 邮件标题
     * @param content html内容
     */
    void sendHtmlMail(String to, String subject, String content);

    /**
     * 发送带附件的邮件
     *
     * @param to       发给谁
     * @param subject  邮件标题
     * @param content  邮件内容
     * @param filePath 附件路径
     */
    void sendAttachmentsMail(String to, String subject, String content, String filePath);

    /**
     * 发送携带静态资源邮件
     *
     * @param to      发给谁
     * @param subject 邮件标题
     * @param content 邮件内容
     * @param rscPath 资源路径
     * @param rscId   资源id
     */
    void sendInlineResourceMail(String to, String subject, String content, String rscPath, String rscId);

    /**
     * 发送模板邮件
     *
     * @param to       发给谁
     * @param subject  标题
     * @param template 邮件模板名称
     * @param context  模板所需参数
     */
    void sendTemplateMail(String to, String subject, String template, Context context);

}
