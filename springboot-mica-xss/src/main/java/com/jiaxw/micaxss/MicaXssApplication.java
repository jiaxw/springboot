package com.jiaxw.micaxss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jiaxw
 * @date 2020/12/3 9:55
 */
@SpringBootApplication
public class MicaXssApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicaXssApplication.class, args);
    }

}
