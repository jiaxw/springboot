package com.jiaxw.micaxss.controller;

import com.jiaxw.micaxss.entity.UserEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * XXS攻击测试类
 *
 * @author jiaxw
 * @date 2020/12/2 11:30
 */
@Api(tags = "XXS攻击测试类")
@Slf4j
@RestController
public class UserRestController {

    @ApiOperation(value = "新增用户")
    @PostMapping("/user")
    public String insert(Long id, Integer age, String name) {
        UserEntity user = new UserEntity();
        user.setId(id);
        user.setAge(age);
        user.setName(name);
        return user.toString();
    }

    @ApiOperation(value = "修改用户")
    @PutMapping("/user")
    public String update(@RequestBody UserEntity user) {
        return user.toString();
    }

    @ApiOperation(value = "删除用户")
    @DeleteMapping("/user")
    public String delete(@RequestBody UserEntity user) {
        return user.toString();
    }

    @ApiOperation(value = "查询用户")
    @GetMapping("/user")
    public String findByUser(UserEntity user) {
        return user.toString();
    }

}
