package com.jiaxw.socket.handler;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.OnConnect;
import com.corundumstudio.socketio.annotation.OnDisconnect;
import com.corundumstudio.socketio.annotation.OnEvent;
import com.jiaxw.socket.entity.MessageEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author jiaxw
 * @date 2020/12/8 15:10
 */
@Slf4j
@Component
public class MessageEventHandler {

    public static SocketIOServer socketIoServer;

    private Map<UUID, String> clientMap = new ConcurrentHashMap<>(16);

    @Autowired
    public MessageEventHandler(SocketIOServer server) {
        MessageEventHandler.socketIoServer = server;
    }

    /**
     * 客户端连接的时候触发
     *
     * @param client
     */
    @OnConnect
    public void onConnect(SocketIOClient client) {
        log.info("【@OnConnect】客户端连接的时候触发");
        // 获取SessionId
        UUID socketSessionId = client.getSessionId();
        // 获取Ip
        String ip = client.getRemoteAddress().toString();
        // 添加到客户端map
        clientMap.put(socketSessionId, ip);
        // 发送客户端ip给前端
        client.sendEvent("getip", ip);
        // 发送上线通知给所有用户
        clientMap.forEach((k, v) -> socketIoServer.getClient(k).sendEvent("refresh", clientMap.values()));
    }

    /**
     * 客户端关闭连接时触发
     *
     * @param client
     */
    @OnDisconnect
    public void onDisconnect(SocketIOClient client) {
        log.info("【@OnDisconnect】客户端关闭连接时触发");
        // 获取SessionId
        UUID socketSessionId = client.getSessionId();
        // 获取Ip
        String ip = client.getRemoteAddress().toString();
        // 从客户端map中移除
        clientMap.remove(socketSessionId);
        // 发送下线通知给所有用户
        clientMap.forEach((k, v) -> socketIoServer.getClient(k).sendEvent("refresh", clientMap.values()));
    }

    /**
     * 客户端事件
     *
     * @param client  客户端信息
     * @param request 请求信息
     * @param message 客户端发送数据
     */
    @OnEvent("message")
    public void message(SocketIOClient client, AckRequest request, MessageEntity message) {
        log.info("【@OnEvent】接收到客户端发送的【message】事件，message：{}", message);
        message.setUsername(client.getRemoteAddress().toString());
        clientMap.forEach((k, v) -> socketIoServer.getClient(k).sendEvent("receiveMsg", message));
    }

}