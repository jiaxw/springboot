package com.jiaxw.socket.entity;

import lombok.Data;

/**
 * @author jiaxw
 * @date 2020/12/8 15:10
 */
@Data
public class MessageEntity {

    private String message;

    private String username;

}
