package com.jiaxw.common.util;

import org.jasypt.util.text.BasicTextEncryptor;

/**
 * jasypt 加密工具类
 *
 * @author jiaxw
 * @date 2020/12/1 17:37
 */
public class JasyptUtil {

    public static void main(String[] args) throws Exception {

        // jasypt
        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword("jiaxw");

        // 加密
        String password = textEncryptor.encrypt("123456");
        System.out.println("password:" + password);

        // 解密
        String originPwd = textEncryptor.decrypt(password);
        System.out.println("originPwd:" + originPwd);
    }

}
