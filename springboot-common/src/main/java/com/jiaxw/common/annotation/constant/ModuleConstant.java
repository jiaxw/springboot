package com.jiaxw.common.annotation.constant;

/**
 * 功能模块常量类
 *
 * @author jiaxw
 * @date 2020/12/3 10:59
 */
public interface ModuleConstant {

    /**
     * 添加
     */
    String POST = "添加";

    /**
     * 修改
     */
    String PUT = "修改";

    /**
     * 删除
     */
    String DELETE = "删除";

    /**
     * 查询
     */
    String GET = "查询";

    /**
     * 上传
     */
    String UPLOAD = "上传";

    /**
     * 下载
     */
    String DOWNLOAD = "下载";

}
