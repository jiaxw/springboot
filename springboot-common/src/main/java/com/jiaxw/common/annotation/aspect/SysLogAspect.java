package com.jiaxw.common.annotation.aspect;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.jiaxw.common.annotation.SysLog;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * 注解日志，异常日志监控所有 Controller 方法
 *
 * @author jiaxw
 * @date 2020/12/3 10:59
 */
@Slf4j
@Aspect
@Component
public class SysLogAspect {

    /**
     * 请求方法类型 GET
     */
    private static final String GET = "GET";

    /**
     * 使用注解位置会切入代码
     */
    @Pointcut("@annotation(com.jiaxw.common.annotation.SysLog)")
    public void sysLogPointCut() {

    }

    /**
     * 设置操作异常切入点记录异常日志 扫描所有controller包下操作
     */
    @Pointcut("execution(public * com.jiaxw.*.controller..*.*(..))")
    public void exceptionLogPointCut() {

    }

    /**
     * 正常返回通知，拦截用户操作日志，连接点正常执行完成后执行，如果连接点抛出异常，则不会执行
     *
     * @param joinPoint 切入点
     * @param keys      返回结果
     */
    @AfterReturning(value = "sysLogPointCut()", returning = "keys")
    public void saveSysLog(JoinPoint joinPoint, Object keys) {

        // 开始时间
        long startTime = System.currentTimeMillis();

        // 获取RequestAttributes
        HttpServletRequest request = getHttpServletRequest();

        try {
            // 从切面织入点处通过反射机制获取织入点处的方法
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            // 获取切入点所在的方法
            Method method = signature.getMethod();
            // 请求ip
            String ip = request.getRemoteAddr();
            // 请求uri
            String uri = request.getRequestURI();
            // 请求url
            String url = request.getRequestURL().toString();
            // 请求方法类型
            String requestMethod = request.getMethod();
            // 获取请求的类名
            String className = joinPoint.getTarget().getClass().getName();
            // 获取请求的方法名
            String methodName = method.getName();
            methodName = className + "." + methodName;
            // 入参
            String params = "";
            // 判断请求方法类型
            if (GET.equalsIgnoreCase(requestMethod)) {
                // 请求的参数
                Map<String, String> paramsMap = converMap(request.getParameterMap());
                // 将参数所在的数组转换成json
                params = JSON.toJSONString(paramsMap);
            } else {
                params = new Gson().toJson(joinPoint.getArgs());
            }
            // 返回结果
            String result = JSON.toJSONString(keys);

            // 获取操作
            SysLog sysLog = method.getAnnotation(SysLog.class);
            String module = sysLog.module();
            String description = sysLog.description();
            boolean console = sysLog.console();
            String replace = sysLog.replace();
            String encryption = sysLog.encryption();

            // 打印日志到控制台
            if (console) {
                printConsole(startTime, ip, uri, url, requestMethod, methodName, params, result);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("日志切面类发生异常:{}", e.getMessage());
        }
    }

    /**
     * 异常返回通知，用于拦截异常日志信息 连接点抛出异常后执行
     *
     * @param joinPoint 切入点
     * @param e         异常信息
     */
    @AfterThrowing(pointcut = "exceptionLogPointCut()", throwing = "e")
    public void saveExceptionLog(JoinPoint joinPoint, Throwable e) {

        // 开始时间
        long startTime = System.currentTimeMillis();

        // 获取RequestAttributes
        HttpServletRequest request = getHttpServletRequest();

        try {
            // 从切面织入点处通过反射机制获取织入点处的方法
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            // 获取切入点所在的方法
            Method method = signature.getMethod();
            // 请求ip
            String ip = request.getRemoteAddr();
            // 请求uri
            String uri = request.getRequestURI();
            // 请求url
            String url = request.getRequestURL().toString();
            // 请求方法类型
            String requestMethod = request.getMethod();
            // 获取请求的类名
            String className = joinPoint.getTarget().getClass().getName();
            // 获取请求的方法名
            String methodName = method.getName();
            methodName = className + "." + methodName;
            // 入参
            String params = "";
            // 判断请求方法类型
            if (GET.equalsIgnoreCase(requestMethod)) {
                // 请求的参数
                Map<String, String> paramsMap = converMap(request.getParameterMap());
                // 将参数所在的数组转换成json
                params = JSON.toJSONString(paramsMap);
            } else {
                params = new Gson().toJson(joinPoint.getArgs());
            }
            // 异常名称
            String exceptionName = e.getClass().getName();
            // 异常信息
            String trace = stackTraceToString(e.getClass().getName(), e.getMessage(), e.getStackTrace());
            // 返回结果
            String result = exceptionName + ":" + trace;

            // 打印日志到控制台
            printConsole(startTime, ip, uri, url, requestMethod, methodName, params, result);
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("异常切面类发生异常:{}", ex.getMessage());
        }
    }

    /**
     * 输出信息到控制台
     *
     * @param startTime     开始时间
     * @param ip            请求ip地址
     * @param uri           接口地址
     * @param url           完整请求路径
     * @param requestMethod 请求方法类型
     * @param methodName    完整方法名称
     * @param params        入参
     * @param result        出参
     */
    private void printConsole(long startTime, String ip, String uri, String url, String requestMethod, String methodName, String params, String result) {
        log.info("========================================== Start ==========================================");
        log.info("IP             : {}", ip);
        log.info("URI            : {}", uri);
        log.info("URL            : {}", url);
        log.info("HTTP Method    : {}", requestMethod);
        log.info("Class Method   : {}", methodName);
        log.info("Request Args   : {}", params);
        log.info("Response Args  : {}", result);
        log.info("=========================================== End ===========================================");
        // 执行耗时
        log.info("Time-Consuming : {} ms", System.currentTimeMillis() - startTime);
    }

    /**
     * 获取 request
     *
     * @return
     */
    private HttpServletRequest getHttpServletRequest() {
        // 获取RequestAttributes
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        // 从获取RequestAttributes中获取HttpServletRequest的信息
        return (HttpServletRequest) requestAttributes.resolveReference(RequestAttributes.REFERENCE_REQUEST);
    }

    /**
     * 转换 request 请求参数
     *
     * @param param request获取的参数数组
     */
    private Map<String, String> converMap(Map<String, String[]> param) {
        Map<String, String> map = new HashMap<>(16);
        for (String key : param.keySet()) {
            map.put(key, param.get(key)[0]);
        }
        return map;
    }

    /**
     * 转换异常信息为字符串
     *
     * @param exceptionName    异常名称
     * @param exceptionMessage 异常信息
     * @param elements         堆栈信息
     */
    private String stackTraceToString(String exceptionName, String exceptionMessage, StackTraceElement[] elements) {
        StringBuffer sb = new StringBuffer();
        for (StackTraceElement ste : elements) {
            sb.append(ste + "\n");
        }
        String message = exceptionName + ":" + exceptionMessage + "\n\t" + sb.toString();
        return message;
    }

}
