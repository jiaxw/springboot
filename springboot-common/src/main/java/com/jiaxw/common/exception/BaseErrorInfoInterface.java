package com.jiaxw.common.exception;

/**
 * 自定义的错误描述枚举类需实现该接口
 *
 * @author jiaxw
 * @date 2020/12/2 11:19
 */
public interface BaseErrorInfoInterface {

    /**
     * 错误码
     *
     * @return
     */
    String getResultCode();

    /**
     * 错误描述
     *
     * @return
     */
    String getResultMsg();

}
