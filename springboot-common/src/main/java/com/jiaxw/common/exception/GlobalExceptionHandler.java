package com.jiaxw.common.exception;

import com.jiaxw.common.vo.ResultCodeEnum;
import com.jiaxw.common.vo.ResultData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 全局异常处理
 *
 * @author jiaxw
 * @date 2020/12/2 11:22
 */
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 处理自定义的业务异常
     *
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value = BizException.class)
    @ResponseBody
    public ResultData bizExceptionHandler(HttpServletRequest request, BizException e) {
        log.error("发生业务异常！原因是：{}", e.getErrorMsg());
        return ResultData.error(e.getErrorCode(), e.getErrorMsg());
    }

    /**
     * 处理空指针的异常
     *
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value = NullPointerException.class)
    @ResponseBody
    public ResultData exceptionHandler(HttpServletRequest request, NullPointerException e) {
        log.error("发生空指针异常！原因是:", e);
        return ResultData.error(ResultCodeEnum.BODY_NOT_MATCH);
    }

    /**
     * 处理其他异常
     *
     * @param request
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResultData exceptionHandler(HttpServletRequest request, Exception e) {
        log.error("未知异常！原因是:", e);
        return ResultData.error(ResultCodeEnum.INTERNAL_SERVER_ERROR);
    }

}
