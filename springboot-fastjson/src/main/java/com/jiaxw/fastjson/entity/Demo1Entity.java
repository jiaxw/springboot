package com.jiaxw.fastjson.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author jiaxw
 * @date 2020/12/11 14:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Demo1Entity {

    private Long id;

    private String name;

    private int age;

    private Date birthday;

    private char sex;

}
