package com.jiaxw.fastjson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jiaxw
 * @date 2020/12/11 14:19
 */
@SpringBootApplication
public class FastJsonApplication {

    public static void main(String[] args) {
        SpringApplication.run(FastJsonApplication.class, args);
    }

}
