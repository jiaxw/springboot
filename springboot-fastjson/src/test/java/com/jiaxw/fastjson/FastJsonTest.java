package com.jiaxw.fastjson;

import com.alibaba.fastjson.JSONArray;
import com.jiaxw.fastjson.entity.Demo1Entity;
import com.jiaxw.fastjson.entity.Demo2Entity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

/**
 * @author jiaxw
 * @date 2020/12/11 14:34
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@SpringBootTest
public class FastJsonTest {

    @Test
    public void test() {
        Demo1Entity demo1Entity = new Demo1Entity(1L, "jiaxw", 25, new Date(), '男');
        String toJSONString = JSONArray.toJSONString(demo1Entity);
        System.out.println(toJSONString);

        Demo2Entity demo2Entity = new Demo2Entity(2L, "jiaxw", 25, new Date());
        String toJSONString2 = JSONArray.toJSONString(demo2Entity);
        System.out.println(toJSONString2);


        // Demo1Entity to Demo2Entity 报错
        /*Demo2Entity demo2Entity1 = (Demo2Entity) JSON.parse(toJSONString);
        System.out.println(JSONArray.toJSONString(demo2Entity1));*/

        // Demo1Entity to Demo2Entity 报错
        /*List<Demo2Entity> demo2Entities = JSONArray.parseArray(toJSONString, Demo2Entity.class);
        for (Demo2Entity entity : demo2Entities) {
            System.out.println(entity);
        }*/

        // Demo2Entity to Demo1Entity 报错
        /*Demo1Entity demo2Entity2 = (Demo1Entity) JSON.parse(toJSONString2);
        System.out.println(JSONArray.toJSONString(demo2Entity2));*/

        // Demo2Entity to Demo1Entity 报错
        /*List<Demo1Entity> demo1Entities = JSONArray.parseArray(toJSONString2, Demo1Entity.class);
        for (Demo1Entity entity : demo1Entities) {
            System.out.println(entity);
        }*/

        // 成功
        Demo2Entity demo2Entity1 = JSONArray.parseObject(toJSONString, Demo2Entity.class);
        System.out.println(demo2Entity1);

        // 成功
        Demo1Entity demo1Entity1 = JSONArray.parseObject(toJSONString2, Demo1Entity.class);
        System.out.println(demo1Entity1);

    }

}
