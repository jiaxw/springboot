package com.jiaxw.aop.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 自定义的错误描述枚举类需实现该接口
 *
 * @author jiaxw
 * @date 2020/12/2 11:25
 */
@ApiModel(value = "用户实体", description = "用户实体")
@Data
public class UserEntity {

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号")
    private Long id;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    private String name;

    /**
     * 年龄
     */
    @ApiModelProperty(value = "年龄")
    private Integer age;

}
