package com.jiaxw.aop.annotation;

import java.lang.annotation.*;

/**
 * 操作日志注解类(可以自己添加属性，实现不同的功能)
 *
 * @author jiaxw
 * @date 2020/12/3 10:59
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SysLog {

    /**
     * 控制台打印入参日志，默认不打印
     *
     * @return
     */
    boolean console() default false;

    /**
     * 操作说明
     *
     * @return
     */
    String description() default "";

    /**
     * 操作模块,参数值:使用 ModuleConstant 常量类{添加，修改，删除，查询，上传，下载}
     *
     * @return
     */
    String module() default "";

    /**
     * 参数(英文)转页面中文显示，格式 name-姓名,age-年龄
     *
     * @return
     */
    String replace() default "";

    /**
     * 关键参数 替换 * ，格式 password,pwd
     *
     * @return
     */
    String encryption() default "";

}
