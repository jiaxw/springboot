package com.jiaxw.easyexcel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.enums.CellExtraTypeEnum;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.alibaba.fastjson.JSON;
import com.jiaxw.easyexcel.entity.CellDataReadDemoData;
import com.jiaxw.easyexcel.entity.ConverterData;
import com.jiaxw.easyexcel.entity.DemoExtraData;
import com.jiaxw.easyexcel.entity.upload.ImportData;
import com.jiaxw.easyexcel.listener.*;
import com.jiaxw.easyexcel.service.impl.ImportDataServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * 导入测试类
 *
 * @author jiaxw
 * @date 2020/12/10 11:29
 */
@Slf4j
@RunWith(value = SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ImportDataTest {

    @Autowired
    private ImportDataServiceImpl importDataService;

    /**
     * 简单读取1
     */
    @Test
    public void simpleRead() {
        // 有个很重要的点 DemoDataListener 不能被spring管理，要每次读取excel都要new,然后里面用到spring可以构造方法传进去
        String fileName = "E:" + File.separator + "号码.xlsx";
        // 这里需要指定读用哪个class去读，然后读取第一个 sheet 文件流会自动关闭
        EasyExcel.read(fileName, ImportData.class, new ImportDataListener(importDataService)).sheet().doRead();
    }

    /**
     * 简单读取2
     */
    @Test
    public void simpleRead2() {
        String fileName = "E:" + File.separator + "号码.xlsx";
        ExcelReader excelReader = null;
        try {
            excelReader = EasyExcel.read(fileName, ImportData.class, new ImportDataListener(importDataService)).build();
            ReadSheet readSheet = EasyExcel.readSheet(0).build();
            excelReader.read(readSheet);
        } finally {
            if (excelReader != null) {
                // 这里千万别忘记关闭，读的时候会创建临时文件，到时磁盘会崩的
                excelReader.finish();
            }
        }
    }

    /**
     * 指定列的下标或者列名
     */
    @Test
    public void indexOrNameRead() {
        String fileName = "E:" + File.separator + "号码.xlsx";
        // 这里默认读取第一个sheet
        EasyExcel.read(fileName, ImportData.class, new ImportDataListener(importDataService)).sheet().doRead();
    }

    /**
     * 读多个或者全部sheet,这里注意一个sheet不能读取多次，多次读取需要重新读取文件
     */
    @Test
    public void repeatedRead() {
        String fileName = "E:" + File.separator + "号码.xlsx";
        // 读取全部sheet
        // 这里需要注意 DemoDataListener的doAfterAllAnalysed 会在每个sheet读取完毕后调用一次。
        // 然后所有sheet都会往同一个DemoDataListener里面写
        EasyExcel.read(fileName, ImportData.class, new ImportDataListener(importDataService)).doReadAll();
    }

    /**
     * 读多个或者全部sheet,这里注意一个sheet不能读取多次，多次读取需要重新读取文件
     */
    @Test
    public void repeatedRead2() {
        // 读取部分sheet
        String fileName = "E:" + File.separator + "号码.xlsx";
        ExcelReader excelReader = null;
        try {
            excelReader = EasyExcel.read(fileName).build();
            // 这里为了简单 所以注册了 同样的head 和Listener 自己使用功能必须不同的Listener
            ReadSheet readSheet1 = EasyExcel.readSheet(0).head(ImportData.class).registerReadListener(new ImportDataListener(importDataService)).build();
            ReadSheet readSheet2 = EasyExcel.readSheet(1).head(ImportData.class).registerReadListener(new ImportDataListener(importDataService)).build();
            // 这里注意 一定要把sheet1 sheet2 一起传进去，不然有个问题就是03版的excel 会读取多次，浪费性能
            excelReader.read(readSheet1, readSheet2);
        } finally {
            if (excelReader != null) {
                // 这里千万别忘记关闭，读的时候会创建临时文件，到时磁盘会崩的
                excelReader.finish();
            }
        }
    }

    /**
     * 日期、数字或者自定义格式转换
     */
    @Test
    public void converterRead() {
        String fileName = "E:" + File.separator + "demo.xlsx";
        EasyExcel.read(fileName, ConverterData.class, new CustomStringConverterListener()).sheet().doRead();
    }

    /**
     * 多行头
     */
    @Test
    public void complexHeaderRead() {
        String fileName = "E:" + File.separator + "demo.xlsx";
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet
        EasyExcel.read(fileName, ConverterData.class, new CustomStringConverterListener()).sheet()
                // 这里可以设置1，因为头就是一行。如果多行头，可以设置其他值。不传入也可以，因为默认会根据DemoData 来解析，他没有指定头，也就是默认1行
                .headRowNumber(1).doRead();
    }

    /**
     * 同步的返回，不推荐使用，如果数据量大会把数据放到内存里面
     */
    @Test
    public void synchronousRead() {
        String fileName = "E:" + File.separator + "demo.xlsx";
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet 同步读取会自动finish
        List<ConverterData> list = EasyExcel.read(fileName).head(ConverterData.class).sheet().doReadSync();
        for (ConverterData data : list) {
            log.info("读取到数据:{}", JSON.toJSONString(data));
        }
    }

    /**
     * 同步的返回，不推荐使用，如果数据量大会把数据放到内存里面
     */
    @Test
    public void synchronousRead2() {
        String fileName = "E:" + File.separator + "demo.xlsx";
        // 这里 也可以不指定class，返回一个list，然后读取第一个sheet 同步读取会自动finish
        List<Map<Integer, String>> listMap = EasyExcel.read(fileName).sheet().doReadSync();
        for (Map<Integer, String> data : listMap) {
            // 返回每条数据的键值对 表示所在的列 和所在列的值
            log.info("读取到数据:{}", JSON.toJSONString(data));
        }
    }

    /**
     * 读取表头数据
     */
    @Test
    public void headerRead() {
        String fileName = "E:" + File.separator + "demo.xlsx";
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet
        EasyExcel.read(fileName, ConverterData.class, new CustomStringConverterListener()).sheet().doRead();
    }

    /**
     * 额外信息（批注、超链接、合并单元格信息读取）
     * 由于是流式读取，没法在读取到单元格数据的时候直接读取到额外信息，所以只能最后通知哪些单元格有哪些额外信息
     */
    @Test
    public void extraRead() {
        String fileName = "E:" + File.separator + "extra.xlsx";
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet
        EasyExcel.read(fileName, DemoExtraData.class, new DemoExtraDataListener())
                // 需要读取批注 默认不读取
                .extraRead(CellExtraTypeEnum.COMMENT)
                // 需要读取超链接 默认不读取
                .extraRead(CellExtraTypeEnum.HYPERLINK)
                // 需要读取合并单元格信息 默认不读取
                .extraRead(CellExtraTypeEnum.MERGE).sheet().doRead();
    }

    /**
     * 读取公式和单元格类型
     */
    @Test
    public void cellDataRead() {
        String fileName = "E:" + File.separator + "cellDataDemo.xlsx";
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet
        EasyExcel.read(fileName, CellDataReadDemoData.class, new CellDataReadDemoDataListener()).sheet().doRead();
    }

    /**
     * 不创建对象的读
     */
    @Test
    public void noModelRead() {
        String fileName = "E:" + File.separator + "号码.xlsx";
        // 这里 只要，然后读取第一个sheet 同步读取会自动finish
        EasyExcel.read(fileName, new NoModelDataListener()).sheet().doRead();
    }


}
