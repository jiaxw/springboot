package com.jiaxw.easyexcel.entity.template;

import lombok.Data;

/**
 * @author jiaxw
 * @date 2020/12/10 19:26
 */
@Data
public class FillData {

    private String name;

    private double number;

}
