package com.jiaxw.easyexcel.entity;

import lombok.Data;

/**
 * @author jiaxw
 * @date 2020/12/10 15:39
 */
@Data
public class DemoExtraData {

    private String row1;

    private String row2;

}
