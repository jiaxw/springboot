package com.jiaxw.easyexcel.entity.upload;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * 导入实体类
 *
 * @author jiaxw
 * @date 2020/12/10 11:24
 */
@Data
public class ImportData {

    /**
     * 强制读取第一个 这里不建议 index 和 name 同时用，要么一个对象只用index，要么一个对象只用name去匹配
     */
    @ExcelProperty(index = 0)
    private String page;

    /**
     * 用名字去匹配，这里需要注意，如果名字重复，会导致只有一个字段读取到数据
     */
    @ExcelProperty(value = "category")
    private String category;

    @ExcelProperty(value = "name")
    private String name;

    @ExcelProperty(value = "address")
    private String address;

    @ExcelProperty(value = "tel")
    private String tel;

    @ExcelProperty(value = "do2")
    private String do2;

    @ExcelProperty(value = "email")
    private String email;

    @ExcelProperty(value = "url")
    private String url;

}
