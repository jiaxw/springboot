package com.jiaxw.easyexcel.controller;

import com.alibaba.excel.EasyExcel;
import com.jiaxw.common.vo.ResultData;
import com.jiaxw.easyexcel.entity.upload.ImportData;
import com.jiaxw.easyexcel.listener.ImportDataListener;
import com.jiaxw.easyexcel.service.impl.ImportDataServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 文件上传
 *
 * @author jiaxw
 * @date 2020/12/10 16:11
 */
@Api(value = "文件上传", tags = {"文件上传"})
@RestController
public class ImportDataController {

    @Autowired
    private ImportDataServiceImpl importDataService;

    /**
     * 文件上传
     *
     * @param file
     * @return
     * @throws IOException
     */
    @ApiOperation(value = "导入")
    @PostMapping("/upload")
    @ResponseBody
    public ResultData upload(MultipartFile file) throws IOException {
        EasyExcel.read(file.getInputStream(), ImportData.class, new ImportDataListener(importDataService)).sheet().doRead();
        return ResultData.ok();
    }

}
