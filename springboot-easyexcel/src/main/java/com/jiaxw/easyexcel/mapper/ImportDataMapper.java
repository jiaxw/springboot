package com.jiaxw.easyexcel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jiaxw.easyexcel.entity.upload.ImportData;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author jiaxw
 * @date 2020/12/10 11:27
 */
@Mapper
public interface ImportDataMapper extends BaseMapper<ImportData> {

}
