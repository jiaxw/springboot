package com.jiaxw.easyexcel.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jiaxw.easyexcel.entity.upload.ImportData;

/**
 * @author jiaxw
 * @date 2020/12/10 11:57
 */
public interface ImportDataService extends IService<ImportData> {

}
