package com.jiaxw.easyexcel.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jiaxw.easyexcel.entity.upload.ImportData;
import com.jiaxw.easyexcel.mapper.ImportDataMapper;
import com.jiaxw.easyexcel.service.ImportDataService;
import org.springframework.stereotype.Service;

/**
 * @author jiaxw
 * @date 2020/12/10 11:58
 */
@Service("importDataService")
public class ImportDataServiceImpl extends ServiceImpl<ImportDataMapper, ImportData> implements ImportDataService {

}
