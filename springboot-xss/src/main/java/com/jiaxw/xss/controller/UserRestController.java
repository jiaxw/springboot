package com.jiaxw.xss.controller;

import com.alibaba.fastjson.JSONArray;
import com.jiaxw.common.vo.ResultData;
import com.jiaxw.xss.entity.UserEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * XXS攻击测试类
 *
 * @author jiaxw
 * @date 2020/12/2 11:30
 */
@Api(tags = "XXS攻击测试类")
@Slf4j
@RestController
public class UserRestController {

    @ApiOperation(value = "新增用户")
    @PostMapping("/user")
    public ResultData insert(Long id, Integer age, String name) {
        UserEntity user = new UserEntity();
        user.setId(id);
        user.setAge(age);
        user.setName(name);
        log.info("新增用户入参：{}", JSONArray.toJSON(user));
        return ResultData.ok().data("user", JSONArray.toJSON(user));
    }

    @ApiOperation(value = "修改用户")
    @PutMapping("/user")
    public ResultData update(@RequestBody UserEntity user) {
        log.info("修改用户入参：{}", JSONArray.toJSON(user));
        return ResultData.ok().data("user", JSONArray.toJSON(user));
    }

    @ApiOperation(value = "删除用户")
    @DeleteMapping("/user")
    public ResultData delete(@RequestBody UserEntity user) {
        log.info("删除用户入参：{}", JSONArray.toJSON(user));
        return ResultData.ok().data("user", JSONArray.toJSON(user));
    }

    @ApiOperation(value = "查询用户")
    @GetMapping("/user")
    public ResultData findByUser(UserEntity user) {
        log.info("查询用户入参：{}", JSONArray.toJSON(user));
        return ResultData.ok().data("user", JSONArray.toJSON(user));
    }

}
