package com.jiaxw.mongodb.util;

import com.jiaxw.mongodb.entity.SysUserEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * MongoDB 测试类
 *
 * @author jiaxw
 * @date 2020/12/1 19:45
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@SpringBootTest
public class MongoUtilTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private MongoUtil mongoUtil;

    @Test
    public void setTest() {
        SysUserEntity user = new SysUserEntity();

        for (int i = 0; i < 50; i++) {
            user.setId(Integer.toUnsignedLong(i));
            user.setUserName("jiaxw" + i);
            SysUserEntity entity = mongoTemplate.insert(user);
            System.out.println(entity);
        }

    }

    @Test
    public void queryLimit() {
        Query query = new Query(new Criteria());
        query.with(Sort.by(Sort.Direction.DESC, "id"));
        Integer currentPage = Math.toIntExact(2);
        Integer pageSize = Math.toIntExact(10);
        mongoUtil.start(currentPage, pageSize, query);
        List<SysUserEntity> list = mongoTemplate.find(query, SysUserEntity.class);
        long count = mongoTemplate.count(query, SysUserEntity.class);
        PageHelper pageHelper = mongoUtil.pageHelper(count, list);
        System.out.println(pageHelper);
    }

}
