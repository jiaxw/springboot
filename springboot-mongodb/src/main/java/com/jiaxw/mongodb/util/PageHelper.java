package com.jiaxw.mongodb.util;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * MongoDB 分页工具类
 *
 * @author jiaxw
 * @date 2020/12/1 19:31
 */
@Data
public class PageHelper<T> {

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private long currentPage;

    /**
     * 总条数
     */
    @ApiModelProperty(value = "总条数")
    private long total;

    /**
     * 每页条数
     */
    @ApiModelProperty(value = "每页条数")
    private long pageSize;

    /**
     * 分页数据
     */
    @ApiModelProperty(value = "分页数据")
    private List<T> list;

    public PageHelper(long pageNum, long total, long pageSize, List<T> list) {
        this.currentPage = pageNum;
        this.total = total;
        this.pageSize = pageSize;
        this.list = list;
    }

    public PageHelper(long pageNum, long pageSize, List<T> list) {
        this.currentPage = pageNum;
        this.pageSize = pageSize;
        this.list = list;
    }

}
