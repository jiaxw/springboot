package com.jiaxw.shiro.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author jiaxw
 * @date 2020/12/9 17:26
 */
@ApiModel(value = "角色对象", description = "角色对象")
@Data
@TableName(value = "role")
public class RoleEntity {

    @ApiModelProperty(value = "主键ID")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @ApiModelProperty(value = "用户对象")
    @TableField(exist = false)
    private UserEntity user;

    @ApiModelProperty(value = "权限集合")
    @TableField(exist = false)
    private List<PermissionEntity> permissionEntities;

}