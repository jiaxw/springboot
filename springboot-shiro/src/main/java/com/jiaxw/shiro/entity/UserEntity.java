package com.jiaxw.shiro.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author jiaxw
 * @date 2020/12/9 17:26
 */
@ApiModel(value = "用户对象", description = "用户对象")
@Data
@TableName(value = "user")
public class UserEntity {

    @ApiModelProperty(value = "主键ID")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty(value = "用户名")
    private String name;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "角色集合")
    @TableField(exist = false)
    private List<RoleEntity> roles;

}