package com.jiaxw.shiro.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author jiaxw
 * @date 2020/12/9 17:26
 */
@ApiModel(value = "权限对象", description = "权限对象")
@Data
@TableName(value = "permission")
public class PermissionEntity {

    @ApiModelProperty(value = "主键ID")
    @TableId(value = "id")
    private Long id;

    @ApiModelProperty(value = "权限")
    private String permission;

    @ApiModelProperty(value = "角色对象")
    @TableField(exist = false)
    private RoleEntity role;

}