package com.jiaxw.shiro.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jiaxw.shiro.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author jiaxw
 * @date 2020-12-09 18:01:25
 */
@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {

}
