package com.jiaxw.shiro.service;

import com.jiaxw.shiro.entity.RoleEntity;
import com.jiaxw.shiro.entity.UserEntity;

/**
 * @author jiaxw
 * @date 2020-12-09 18:01:25
 */
public interface LoginService {

    /**
     * 添加用户
     *
     * @param user
     * @return
     */
    UserEntity addUser(UserEntity user);

    /**
     * 添加角色
     *
     * @param role
     * @return
     */
    RoleEntity addRole(RoleEntity role);

    /**
     * 根据姓名查询用户
     *
     * @param name
     * @return
     */
    UserEntity findByName(String name);

}