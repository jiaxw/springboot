package com.jiaxw.shiro.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jiaxw.shiro.entity.PermissionEntity;
import com.jiaxw.shiro.entity.RoleEntity;
import com.jiaxw.shiro.entity.UserEntity;
import com.jiaxw.shiro.mapper.RoleMapper;
import com.jiaxw.shiro.mapper.UserMapper;
import com.jiaxw.shiro.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jiaxw
 * @date 2020-12-09 18:01:25
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public UserEntity addUser(UserEntity user) {
        userMapper.insert(user);
        return user;
    }

    @Override
    public RoleEntity addRole(RoleEntity role) {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("role_name", role.getUser().getName());
        UserEntity user = userMapper.selectOne(wrapper);
        role.setUser(user);
        PermissionEntity permissionEntity1 = new PermissionEntity();
        permissionEntity1.setPermission("create");
        permissionEntity1.setRole(role);
        PermissionEntity permissionEntity2 = new PermissionEntity();
        permissionEntity2.setPermission("update");
        permissionEntity2.setRole(role);
        List<PermissionEntity> permissionEntities = new ArrayList<>();
        permissionEntities.add(permissionEntity1);
        permissionEntities.add(permissionEntity2);
        role.setPermissionEntities(permissionEntities);
        roleMapper.insert(role);
        return role;
    }

    @Override
    public UserEntity findByName(String name) {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("name", name);
        return userMapper.selectOne(wrapper);
    }

}