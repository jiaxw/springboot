package com.jiaxw.shiro.controller;

import com.jiaxw.common.vo.ResultData;
import com.jiaxw.shiro.entity.RoleEntity;
import com.jiaxw.shiro.entity.UserEntity;
import com.jiaxw.shiro.service.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Shiro 测试类
 *
 * @author jiaxw
 * @date 2020-12-09 18:01:25
 */
@Api(value = "登录", tags = {"登录"})
@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    /**
     * POST登录
     *
     * @return
     */
    @ApiOperation(value = "登录")
    @PostMapping(value = "/login")
    public ResultData login(@RequestBody UserEntity user) {
        // 添加用户认证信息
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(user.getName(), user.getPassword());
        // 进行验证，这里可以捕获异常，然后返回对应信息
        SecurityUtils.getSubject().login(usernamePasswordToken);
        return ResultData.ok();
    }

    /**
     * 添加用户
     *
     * @param user
     * @return
     */
    @ApiOperation(value = "添加用户")
    @PostMapping(value = "/addUser")
    public ResultData addUser(@RequestBody UserEntity user) {
        user = loginService.addUser(user);
        return ResultData.ok().data("user", user);
    }

    /**
     * 添加角色
     *
     * @param role
     * @return
     */
    @ApiOperation(value = "添加角色")
    @PostMapping(value = "/addRole")
    public ResultData addRole(@RequestBody RoleEntity role) {
        role = loginService.addRole(role);
        return ResultData.ok().data("user", role);
    }

    /**
     * 注解的使用
     *
     * @return
     */
    @ApiOperation(value = "create")
    @RequiresRoles("admin")
    @RequiresPermissions("create")
    @GetMapping(value = "/create")
    public ResultData create() {
        return ResultData.ok();
    }

    @ApiOperation(value = "index")
    @GetMapping(value = "/index")
    public String index() {
        return "index page!";
    }

    @ApiOperation(value = "error")
    @GetMapping(value = "/error")
    public String error() {
        return "error page!";
    }

    /**
     * 退出的时候是get请求，主要是用于退出
     *
     * @return
     */
    @ApiOperation(value = "login")
    @GetMapping(value = "/login")
    public String login() {
        return "login";
    }

    @ApiOperation(value = "logout")
    @GetMapping(value = "/logout")
    public String logout() {
        return "logout";
    }

}