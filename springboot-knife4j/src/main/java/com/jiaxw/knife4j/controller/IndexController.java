package com.jiaxw.knife4j.controller;

import com.jiaxw.common.vo.ResultData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jiaxw
 * @date 2020/12/1 14:22
 */
@Api(tags = "首页模块")
@RestController
public class IndexController {

    @ApiImplicitParam(name = "name", value = "姓名", required = true)
    @ApiOperation(value = "SpringBoot 整合 Knife4j")
    @GetMapping("/index")
    public ResultData index(@RequestParam(value = "name") String name) {
        return ResultData.ok().data("SpringBoot 整合 Knife4j", name);
    }

}
