package com.jiaxw.knife4j;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jiaxw
 * @date 2020/12/1 14:21
 */
@SpringBootApplication
public class Knife4jApplication {

    public static void main(String[] args) {
        SpringApplication.run(Knife4jApplication.class, args);
    }

}
