package com.jiaxw.security.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 登录接口封装对象
 *
 * @author jiaxw
 * @date 2020/12/9 14:01
 */
@ApiModel(value = "登录接口封装对象", description = "登录接口封装对象")
@Data
public class LoginBean {

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

}
