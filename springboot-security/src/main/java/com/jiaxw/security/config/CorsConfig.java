package com.jiaxw.security.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 跨域配置
 *
 * @author jiaxw
 * @date 2020/12/9 14:01
 */
@Configuration
public class CorsConfig implements WebMvcConfigurer {

    /**
     * 允许请求的方法
     */
    private static final String[] METHODS = {"POST", "GET", "PUT", "OPTIONS", "DELETE"};

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        // 允许跨域访问的路径
        registry.addMapping("/**")
                // 允许跨域访问的源
                .allowedOrigins("*")
                // 允许请求方法
                .allowedMethods(METHODS)
                // 预检间隔时间
                .maxAge(168000)
                // 允许头部设置
                .allowedHeaders("*")
                // 是否发送 cookie
                .allowCredentials(true);
    }

}