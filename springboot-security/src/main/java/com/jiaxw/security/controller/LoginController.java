package com.jiaxw.security.controller;

import com.jiaxw.common.vo.ResultData;
import com.jiaxw.security.security.JwtAuthenticationToken;
import com.jiaxw.security.utils.SecurityUtils;
import com.jiaxw.security.vo.LoginBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 自定义登录控制器(使用自定义的需要去 WebSecurityConfig 注释一些配置)
 *
 * @author jiaxw
 * @date 2020/12/9 14:01
 */
@Api(value = "登录", tags = {"登录"})
@RestController
public class LoginController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @ApiOperation(value = "登录")
    @PostMapping(value = "/login")
    public ResultData login(@RequestBody LoginBean loginBean, HttpServletRequest request) throws IOException {

        String username = loginBean.getUsername();
        String password = loginBean.getPassword();

        // 系统登录认证
        JwtAuthenticationToken token = SecurityUtils.login(request, username, password, authenticationManager);

        return ResultData.ok().data("token", token);
    }

}
