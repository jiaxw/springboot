package com.jiaxw.security.controller;

import com.jiaxw.common.vo.ResultData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Security 测试类
 *
 * @author jiaxw
 * @date 2020/12/9 14:01
 */
@Api(value = "Security 测试类", tags = {"Security 测试类"})
@RestController
@RequestMapping("/user")
public class UserController {

    @ApiOperation(value = "查看")
    @PreAuthorize("hasAuthority('sys:user:view')")
    @GetMapping(value = "/findAll")
    public ResultData findAll() {
        return ResultData.ok();
    }

    @ApiOperation(value = "修改")
    @PreAuthorize("hasAuthority('sys:user:edit')")
    @GetMapping(value = "/edit")
    public ResultData edit() {
        return ResultData.ok();
    }

    @ApiOperation(value = "删除")
    @PreAuthorize("hasAuthority('sys:user:delete')")
    @GetMapping(value = "/delete")
    public ResultData delete() {
        return ResultData.ok();
    }

    @ApiOperation(value = "没有权限")
    @PreAuthorize("hasAuthority('sys:user:get')")
    @GetMapping(value = "/get")
    public ResultData get() {
        return ResultData.ok();
    }

}
