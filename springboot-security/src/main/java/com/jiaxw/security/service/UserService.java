package com.jiaxw.security.service;

import com.jiaxw.security.entity.UserEntity;

import java.util.Set;

/**
 * @author jiaxw
 * @date 2020/12/9 14:01
 */
public interface UserService {

    /**
     * 根据用户名查找用户
     *
     * @param username
     * @return
     */
    UserEntity findByUsername(String username);

    /**
     * 查找用户的菜单权限标识集合
     *
     * @param username
     * @return
     */
    Set<String> findPermissions(String username);

}
