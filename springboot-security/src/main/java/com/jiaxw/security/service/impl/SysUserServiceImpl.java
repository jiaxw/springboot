package com.jiaxw.security.service.impl;

import com.jiaxw.security.entity.UserEntity;
import com.jiaxw.security.service.UserService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * @author jiaxw
 * @date 2020/12/9 14:01
 */
@Service
public class SysUserServiceImpl implements UserService {

    @Override
    public UserEntity findByUsername(String username) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(1L);
        userEntity.setUsername(username);
        String password = new BCryptPasswordEncoder().encode("123");
        userEntity.setPassword(password);
        return userEntity;
    }

    @Override
    public Set<String> findPermissions(String username) {
        Set<String> permissions = new HashSet<>();
        permissions.add("sys:user:view");
        permissions.add("sys:user:add");
        permissions.add("sys:user:edit");
        permissions.add("sys:user:delete");
        return permissions;
    }

}
