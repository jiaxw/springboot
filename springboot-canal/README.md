# Canal
使用逻辑，根据表名把数据进行分割，按不同的表来操作 增删改 会使代码更简洁。

## 开源地址
https://github.com/alibaba/canal.git

## 参考文章
https://blog.csdn.net/boonya/article/details/89406067

https://blog.csdn.net/leilei1366615/article/details/108819651