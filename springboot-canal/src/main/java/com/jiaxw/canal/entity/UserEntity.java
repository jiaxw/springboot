package com.jiaxw.canal.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 第二种方式实现
 *
 * @author jiaxw
 * @date 2020/12/11 16:40
 */
@Data
@Table(name = "t_user")
public class UserEntity implements Serializable {

    /**
     * 主键
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 用户名
     */
    @Column(name = "user_name")
    private String userName;

    /**
     * 用户性别
     */

    private Integer gender;

    /**
     * 国家id
     */
    @Column(name = "country_id")
    private Integer countryId;

    /**
     * 用户出生日期
     */
    private Date birthday;

    /**
     * 用户创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

}