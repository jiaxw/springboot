package com.jiaxw.canal;

import com.jiaxw.canal.handler.CanalClientHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jiaxw
 * @date 2020/12/11 16:01
 */
@SpringBootApplication
public class CanalApplication {

    public static void main(String[] args) {
        SpringApplication.run(CanalApplication.class, args);

        // 启动 Canal
        CanalClientHandler.start();
    }

}
