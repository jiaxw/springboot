package com.jiaxw.canal.handler;

import com.jiaxw.canal.entity.UserEntity;
import lombok.extern.slf4j.Slf4j;
import top.javatool.canal.client.handler.EntryHandler;

/**
 * // TODO 第二种方式实现
 * 注释
 *
 * @author jiaxw
 * @date 2020/12/11 16:40
 */
@Slf4j
//@Component
//@CanalTable(value = "t_user")
public class UserHandler implements EntryHandler<UserEntity> {

    @Override
    public void insert(UserEntity user) {
        log.info("insert message  {}", user);
    }

    @Override
    public void update(UserEntity before, UserEntity after) {
        log.info("update before {} ", before);
        log.info("update after {}", after);
    }

    @Override
    public void delete(UserEntity user) {
        log.info("delete  {}", user);
    }

}
