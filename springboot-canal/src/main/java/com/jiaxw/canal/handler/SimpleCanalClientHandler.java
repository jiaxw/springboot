package com.jiaxw.canal.handler;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.otter.canal.client.CanalConnector;
import com.alibaba.otter.canal.client.CanalConnectors;
import com.alibaba.otter.canal.protocol.CanalEntry;
import com.alibaba.otter.canal.protocol.Message;
import com.google.protobuf.InvalidProtocolBufferException;
import com.jiaxw.canal.entity.UserEntity;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Canal 第三种实现方式(推荐使用)
 *
 * @author jiaxw
 * @date 2020/12/14 10:43
 */
@Slf4j
public class SimpleCanalClientHandler {

    private static String SERVER_ADDRESS = "192.168.253.128";
    private static Integer PORT = 11111;
    private static String DESTINATION = "example";
    private static String USERNAME = "canal";
    private static String PASSWORD = "canal";

    public static void start() {
        // 创建链接
        CanalConnector connector = CanalConnectors.newSingleConnector(
                new InetSocketAddress(SERVER_ADDRESS, PORT), DESTINATION, USERNAME, PASSWORD);
        int batchSize = 1000;
        try {
            connector.connect();
            connector.subscribe(".*\\..*");
            connector.rollback();
            while (true) {
                // 获取指定数量的数据
                Message message = connector.getWithoutAck(batchSize);
                long batchId = message.getId();
                int size = message.getEntries().size();
                if (batchId == -1 || size == 0) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                } else {
                    printEntity(message.getEntries());
                }
                // 提交确认
                connector.ack(batchId);
                // connector.rollback(batchId); // 处理失败, 回滚数据
            }
        } finally {
            connector.disconnect();
        }
    }

    public static void printEntity(List<CanalEntry.Entry> entries) {
        for (CanalEntry.Entry entry : entries) {
            if (entry.getEntryType() != CanalEntry.EntryType.ROWDATA) {
                continue;
            }
            try {
                CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
                for (CanalEntry.RowData rowData : rowChange.getRowDatasList()) {
                    String tableName = entry.getHeader().getTableName();
                    switch (rowChange.getEventType()) {
                        case INSERT:
                            // 测试选用 t_user 这张表进行映射处理
                            if ("t_user".equals(tableName)) {
                                UserEntity userEntity = CanalDataHandler.convertToBean(rowData.getAfterColumnsList(), UserEntity.class);
                                log.info("t_user 新增数据:{}", JSONArray.toJSONString(userEntity));
                            }
                            break;
                        case UPDATE:
                            if ("t_user".equals(tableName)) {
                                UserEntity userEntity1 = CanalDataHandler.convertToBean(rowData.getBeforeColumnsList(), UserEntity.class);
                                UserEntity userEntity2 = CanalDataHandler.convertToBean(rowData.getAfterColumnsList(), UserEntity.class);
                                log.info("t_user 修改数据之前的数据:{}", JSONArray.toJSONString(userEntity1));
                                log.info("t_user 修改数据之后的数据:{}", JSONArray.toJSONString(userEntity2));
                            }
                            break;
                        default:
                            break;
                    }
                }
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
                log.error("printEntity 异常信息：{}", e);
            }
        }
    }

    /**
     * 打印内容
     *
     * @param columns
     */
    private static void printColumns(List<CanalEntry.Column> columns) {
        String line = columns.stream().map(column -> column.getName() + "=" + column.getValue())
                .collect(Collectors.joining(","));
        log.info(line);
    }

    public static void main(String[] args) {
        start();
    }
}
