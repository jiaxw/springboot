# easypoi 报表工具

## 本地文档地址
http://127.0.0.1:8710/doc.html

## druid数据库连接池
http://127.0.0.1:8710/druid/login.html


## gitee 开源地址
https://gitee.com/lemur/easypoi.git

## 对接文档
http://doc.wupaas.com/docs/easypoi/easypoi-1c0u4mo8p4ro8

## 测试demo
https://gitee.com/lemur/easypoi-test

## 版本升级
https://gitee.com/lemur/easypoi/blob/master/history.md