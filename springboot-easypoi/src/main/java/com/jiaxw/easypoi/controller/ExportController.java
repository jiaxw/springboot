package com.jiaxw.easypoi.controller;

import cn.afterturn.easypoi.excel.entity.ExportParams;
import com.jiaxw.easypoi.entity.BlackListExport;
import com.jiaxw.easypoi.util.EasyPoiUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 导出测试类
 *
 * @author jiaxw
 * @date 2020/12/11 11:14
 */
@Api(value = "导出测试类", tags = {"导出测试类"})
@RestController
public class ExportController {

    @ApiOperation(value = "easyPoiUtil 导出测试")
    @GetMapping(value = "/poi/export1")
    public void export1(HttpServletResponse response) {
        List<BlackListExport> list = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            list.add(new BlackListExport("name" + i, "remark" + i + "", "phone" + i));
        }
        // 没有标题
        EasyPoiUtil.exportExcel(list, null, "jiaxw", BlackListExport.class, "jiaxw.xls", response);
        // 设置标题
        EasyPoiUtil.exportExcel(list, "jiaxw", "jiaxw", BlackListExport.class, "jiaxw2.xls", response);
    }

    /**
     * 如果填充不同sheet得data数据列表使用相同得list对象进行填充的话，
     * 会出现第一次填充得sheet有数据，后续其他使用相同list对象进行data填充得sheet没有数据展示。
     *
     * @param response
     */
    @ApiOperation(value = "多sheet 导出测试")
    @GetMapping(value = "/poi/export2")
    public void export2(HttpServletResponse response) {
        // 查询数据,此处省略
        List list = new ArrayList<>();
        list.add(new BlackListExport("姓名1", "备注1", "手机1"));
        list.add(new BlackListExport("姓名2", "备注2", "手机2"));
        list.add(new BlackListExport("姓名3", "备注3", "手机3"));
        List list2 = new ArrayList<>();
        list2.add(new BlackListExport("姓名-1", "备注-1", "手机-1"));
        list2.add(new BlackListExport("姓名-2", "备注-2", "手机-2"));
        list2.add(new BlackListExport("姓名-3", "备注-3", "手机-3"));
        List<Map<String, Object>> sheetsList = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            // 设置导出配置
            // 创建参数对象（用来设定excel得sheet得内容等信息）
            ExportParams params = new ExportParams();
            // 设置sheet得名称
            params.setSheetName("表格" + i);

            //创建sheet使用的map
            Map<String, Object> dataMap = new HashMap<>();
            // title的参数为ExportParams类型，目前仅仅在ExportParams中设置了sheetName
            dataMap.put("title", params);
            // 模版导出对应得实体类型
            dataMap.put("entity", BlackListExport.class);
            // sheet中要填充得数据
            if (i % 2 == 0) {
                dataMap.put("data", list);
            } else {
                dataMap.put("data", list2);
            }

            sheetsList.add(dataMap);
        }
        EasyPoiUtil.exportExcel(sheetsList, "jiaxw3.xls", response);
    }

}
