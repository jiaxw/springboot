package com.jiaxw.easypoi.controller;

import com.jiaxw.common.vo.ResultData;
import com.jiaxw.easypoi.entity.BlackListExport;
import com.jiaxw.easypoi.util.EasyPoiUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * 导入测试类
 *
 * @author jiaxw
 * @date 2020/12/11 11:47
 */
@Api(value = "导入测试类", tags = {"导入测试类"})
@RestController
public class ImportController {

    @ApiOperation(value = "easyPoiUtil 导入测试")
    @PostMapping("/upload")
    @ResponseBody
    public ResultData upload(MultipartFile file) throws IOException {
        final List<BlackListExport> list = EasyPoiUtil.importExcel(file, 0, 1, BlackListExport.class);
        for (BlackListExport blackListExport : list) {
            System.out.println(blackListExport);
        }
        System.out.println(list.size());
        return ResultData.ok().data("list", list);
    }

}
