package com.jiaxw.easypoi.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author jiaxw
 * @date 2020/12/11 11:13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlackListExport {

    @Excel(name = "客户姓名", width = 15, orderNum = "2")
    private String name;

    @Excel(name = "备注", width = 10, orderNum = "1")
    private String remark;

    @Excel(name = "手机号", width = 15, orderNum = "0")
    private String phone;

}


