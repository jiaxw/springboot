package com.jiaxw.easypoi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jiaxw
 * @date 2020/12/9 22:29
 */
@SpringBootApplication
public class EasyPoiApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasyPoiApplication.class, args);
    }

}