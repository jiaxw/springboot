package com.jiaxw.mybatisplus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.jiaxw.common.annotation.aspect.SysLogAspect;
import com.jiaxw.common.exception.GlobalExceptionHandler;
import com.jiaxw.common.xss.XssFilter;
import com.jiaxw.common.xss.XssStringJsonSerializer;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

/**
 * @author jiaxw
 * @date 2020/12/1 15:55
 */
@SpringBootApplication
@MapperScan(basePackages = {"com.jiaxw.mybatisplus.mapper"})
public class MybatisPlusApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisPlusApplication.class, args);
    }

    /**
     * 从 common 中注入全局异常处理类
     *
     * @return
     */
    @Bean
    public GlobalExceptionHandler globalExceptionHandler() {
        return new GlobalExceptionHandler();
    }

    /**
     * 注入 xss过滤器，只能防止表单提交，要是json还需要注入下面一个bean
     *
     * @return
     */
    @Bean
    public XssFilter xssFilter() {
        return new XssFilter();
    }

    /**
     * 注入 json 转换
     *
     * @param builder
     * @return
     */
    @Bean
    @Primary
    public ObjectMapper xssObjectMapper(Jackson2ObjectMapperBuilder builder) {
        // 解析器
        ObjectMapper objectMapper = builder.createXmlMapper(false).build();
        // 注册 xss 解析器
        SimpleModule xssModule = new SimpleModule("XssStringJsonSerializer");
        xssModule.addSerializer(new XssStringJsonSerializer());
        objectMapper.registerModule(xssModule);
        // 返回
        return objectMapper;
    }

    /**
     * 注入日志切面
     *
     * @return
     */
    @Bean
    public SysLogAspect sysLogAspect() {
        return new SysLogAspect();
    }

}
