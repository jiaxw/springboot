package com.jiaxw.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jiaxw.mybatisplus.entity.SysUserEntity;

/**
 * 系统用户mapper
 *
 * @author jiaxw
 * @date 2020/12/1 15:55
 */
public interface SysUserMapper extends BaseMapper<SysUserEntity> {

}
