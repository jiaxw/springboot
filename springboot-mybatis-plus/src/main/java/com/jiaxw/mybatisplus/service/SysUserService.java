package com.jiaxw.mybatisplus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jiaxw.mybatisplus.entity.SysUserEntity;

/**
 * 系统用户Service
 *
 * @author jiaxw
 * @date 2020/12/1 15:55
 */
public interface SysUserService extends IService<SysUserEntity> {

}
