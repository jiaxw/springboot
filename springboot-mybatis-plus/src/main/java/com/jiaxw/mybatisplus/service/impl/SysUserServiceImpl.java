package com.jiaxw.mybatisplus.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jiaxw.mybatisplus.entity.SysUserEntity;
import com.jiaxw.mybatisplus.mapper.SysUserMapper;
import com.jiaxw.mybatisplus.service.SysUserService;
import org.springframework.stereotype.Service;

/**
 * 系统用户Service
 *
 * @author jiaxw
 * @date 2020/12/1 15:55
 */
@Service("sysUserService")
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUserEntity> implements SysUserService {

}
