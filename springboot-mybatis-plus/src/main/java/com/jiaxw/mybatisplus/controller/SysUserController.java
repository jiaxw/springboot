package com.jiaxw.mybatisplus.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiaxw.common.vo.ResultData;
import com.jiaxw.mybatisplus.entity.SysUserEntity;
import com.jiaxw.mybatisplus.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统用户Controller
 *
 * @author jiaxw
 * @date 2020/12/1 15:55
 */
@Api(tags = "系统用户")
@Slf4j
@RestController
@RequestMapping("/sys/user")
public class SysUserController {

    @Autowired
    private SysUserService userService;

    /**
     * 查询所有用户列表
     *
     * @return
     */
    @GetMapping("/findAll")
    @ApiOperation(value = "查询所有用户列表")
    public ResultData findAllUser() {
        List<SysUserEntity> userList = userService.list(null);
        return ResultData.ok().data("items", userList);
    }

    /**
     * 分页查询用户信息
     *
     * @param current
     * @param limit
     * @return
     */
    @GetMapping("/pageListUser/{current}/{limit}")
    @ApiOperation(value = "分页查询用户信息")
    public ResultData pageListUser(@PathVariable long current, @PathVariable long limit) {
        Page<SysUserEntity> userPage = new Page<>(current, limit);
        userService.page(userPage, null);
        long total = userPage.getTotal();
        List<SysUserEntity> records = userPage.getRecords();
        return ResultData.ok().data("total", total).data("rows", records);
    }

    /**
     * 新增用户信息
     *
     * @param user
     * @return
     */
    @PostMapping("/add")
    @ApiOperation(value = "新增用户")
    public ResultData add(@RequestBody SysUserEntity user) {

        user.setPassword(user.getPassword());
        boolean flag = userService.save(user);

        if (flag) {
            return ResultData.ok();
        } else {
            return ResultData.error();
        }
    }

    /**
     * 根据ID查询用户信息
     *
     * @param id
     * @return
     */
    @GetMapping("getUserById/{id}")
    @ApiOperation(value = "根据ID查询用户信息")
    public ResultData getUserById(@PathVariable Long id) {
        SysUserEntity user = userService.getById(id);
        return ResultData.ok().data("user", user);
    }

    /**
     * 修改用户信息
     *
     * @param user
     * @return
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改用户")
    public ResultData update(@RequestBody SysUserEntity user) {
        userService.updateById(user);
        return ResultData.ok();
    }

    /**
     * 逻辑删除用户信息
     *
     * @param id
     * @return
     */
    @DeleteMapping("/remove/{id}")
    @ApiOperation(value = "逻辑删除用户信息")
    public ResultData remove(@ApiParam(name = "id", value = "主键ID", required = true) @PathVariable Long id) {
        boolean flag = userService.removeById(id);
        if (flag) {
            return ResultData.ok();
        } else {
            return ResultData.error();
        }
    }

}

