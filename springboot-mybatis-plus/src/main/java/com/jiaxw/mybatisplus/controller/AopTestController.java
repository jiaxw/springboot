package com.jiaxw.mybatisplus.controller;

import com.alibaba.fastjson.JSONArray;
import com.jiaxw.common.annotation.SysLog;
import com.jiaxw.common.annotation.constant.ModuleConstant;
import com.jiaxw.common.exception.BizException;
import com.jiaxw.common.vo.ResultData;
import com.jiaxw.mybatisplus.entity.UserEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * Aop 测试类
 *
 * @author jiaxw
 * @date 2020/12/2 11:30
 */
@Api(tags = "Aop 测试类")
@Slf4j
@RestController
@RequestMapping("/aop")
public class AopTestController {

    @SysLog(module = ModuleConstant.POST, description = "新增")
    @ApiOperation(value = "新增用户")
    @PostMapping("/user")
    public ResultData insert(Long id, Integer age, String name) {
        UserEntity user = new UserEntity();
        user.setId(id);
        user.setAge(age);
        user.setName(name);

        // 如果姓名为空就手动抛出一个自定义的异常！
        if (StringUtils.isBlank(user.getName())) {
            throw new BizException("-1", "用户姓名不能为空！");
        }

        log.info("新增用户入参：{}", JSONArray.toJSON(user));
        return ResultData.ok().data("user", JSONArray.toJSON(user));
    }

    @SysLog(module = ModuleConstant.PUT, description = "修改", console = true)
    @ApiOperation(value = "修改用户")
    @PutMapping("/user")
    public ResultData update(@RequestBody UserEntity user) {
        log.info("修改用户入参：{}", JSONArray.toJSON(user));
        return ResultData.ok().data("user", JSONArray.toJSON(user));
    }

    @SysLog(module = ModuleConstant.DELETE, description = "删除", console = true)
    @ApiOperation(value = "删除用户")
    @DeleteMapping("/user")
    public ResultData delete(@RequestBody UserEntity user) {
        log.info("删除用户入参：{}", JSONArray.toJSON(user));
        return ResultData.ok().data("user", JSONArray.toJSON(user));
    }

    @SysLog(module = ModuleConstant.GET, description = "查询")
    @ApiOperation(value = "查询用户")
    @GetMapping("/user")
    public ResultData findByUser(UserEntity user) {
        log.info("查询用户入参：{}", JSONArray.toJSON(user));
        return ResultData.ok().data("user", JSONArray.toJSON(user));
    }

}
