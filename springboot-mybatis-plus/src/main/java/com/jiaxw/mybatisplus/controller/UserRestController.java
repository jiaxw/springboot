package com.jiaxw.mybatisplus.controller;

import com.jiaxw.common.exception.BizException;
import com.jiaxw.mybatisplus.entity.UserEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 接口控制类
 *
 * @author jiaxw
 * @date 2020/12/2 11:30
 */
@Api(tags = "全局异常测试控制类")
@RestController
@RequestMapping(value = "/api")
public class UserRestController {

    @ApiOperation(value = "新增用户")
    @PostMapping("/user")
    public boolean insert(@RequestBody UserEntity user) {
        System.out.println("开始新增...");
        // 如果姓名为空就手动抛出一个自定义的异常！
        if (StringUtils.isBlank(user.getName())) {
            throw new BizException("-1", "用户姓名不能为空！");
        }
        return true;
    }

    @ApiOperation(value = "更新用户")
    @PutMapping("/user")
    public boolean update(@RequestBody UserEntity user) {
        System.out.println("开始更新...");
        // 这里故意造成一个空指针的异常，并且不进行处理
        String str = null;
        str.equals("111");
        return true;
    }

    @ApiOperation(value = "删除用户")
    @DeleteMapping("/user")
    public boolean delete(@RequestBody UserEntity user) {
        System.out.println("开始删除...");
        // 这里故意造成一个异常，并且不进行处理
        Integer.parseInt("abc123");
        return true;
    }

    @ApiOperation(value = "查询用户")
    @GetMapping("/user")
    public List<UserEntity> findByUser(UserEntity user) {
        System.out.println("开始查询...");
        List<UserEntity> userList = new ArrayList<>();
        UserEntity user2 = new UserEntity();
        user2.setId(1L);
        user2.setName("jiaxw");
        user2.setAge(25);
        userList.add(user2);
        return userList;
    }

}
