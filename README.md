# 权限认证
## Shiro
## Spring Security
## Spring Security OAuth2

# 消息队列
## ActiveMQ
## Kafka
## RabbitMQ
## RocketMQ
## ZeroMQ


# 阿里巴巴开源项目

https://github.com/alibaba/spring-cloud-alibaba.git

https://github.com/alibaba/nacos.git

https://github.com/alibaba/Sentinel.git

https://github.com/seata/seata.git

https://github.com/alibaba/druid.git

https://github.com/alibaba/fastjson.git

https://github.com/alibaba/canal.git

https://github.com/alibaba/easyexcel.git

https://github.com/alibaba/arthas.git


# 码云GVP项目

## 权限系统
https://gitee.com/smallc/SpringBlade.git

https://gitee.com/renrenio/renren-security.git

https://gitee.com/y_project/RuoYi.git

https://gitee.com/xuxueli0323/xxl-sso.git

https://gitee.com/log4j/pig.git

https://gitee.com/shuzheng/zheng.git

https://gitee.com/stylefeng/guns.git

https://gitee.com/geek_qi/cloud-platform.git

https://gitee.com/fuyang_lipengjun/platform.git

https://gitee.com/owenwangwen/open-capacity-platform.git

https://gitee.com/JbootProjects/jboot.git

https://gitee.com/jishenghua/JSH_ERP.git

https://gitee.com/iBase4J/iBase4J.git

https://gitee.com/thinkgem/jeesite.git

https://gitee.com/bweird/lenosp.git

https://gitee.com/52itstyle/spring-boot-seckill.git

https://gitee.com/yadong.zhang/JustAuth.git

## 工具
https://gitee.com/baomidou/mybatis-plus.git

https://gitee.com/xiaoym/knife4j.git

https://gitee.com/lemur/easypoi.git

https://gitee.com/loolly/hutool.git

https://gitee.com/OpenSkywalking/sky-walking.git

https://gitee.com/arthas/arthas.git

https://gitee.com/kekingcn/file-online-preview.git

https://gitee.com/lionsoul/jcseg.git

https://gitee.com/binary/weixin-java-tools.git

## 定时任务
https://gitee.com/xuxueli0323/xxl-job.git

https://gitee.com/elasticjob/elastic-job.git

## socket
https://gitee.com/smartboot/smart-socket.git

## 教学系统
https://gitee.com/mindskip/uexam.git

## 支付
https://gitee.com/javen205/IJPay.git

https://gitee.com/roncoocom/roncoo-pay.git

https://gitee.com/jmdhappy/xxpay-master.git

## 区块链
https://gitee.com/tianyalei/md_blockchain.git

## 爬虫
https://gitee.com/l-weiwei/Spiderman2.git

https://gitee.com/flashsword20/webmagic.git

## 前端项目
https://gitee.com/xuliangzhan_admin/vxe-table.git

https://gitee.com/uCharts/uCharts.git


