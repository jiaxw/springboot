package com.jiaxw.generator.config;

import com.jiaxw.generator.mapper.*;
import com.jiaxw.generator.utils.RRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class DbConfig {

    @Value("${generator.database: mysql}")
    private String database;

    @Autowired
    private MySQLGeneratorMapper mySQLGeneratorMapper;

    @Autowired
    private OracleGeneratorMapper oracleGeneratorMapper;

    @Autowired
    private SQLServerGeneratorMapper sqlServerGeneratorMapper;

    @Autowired
    private PostgreSQLGeneratorMapper postgreSQLGeneratorMapper;

    @Bean
    @Primary
    public GeneratorMapper getGeneratorMapper() {
        if ("mysql".equalsIgnoreCase(database)) {
            return mySQLGeneratorMapper;
        } else if ("oracle".equalsIgnoreCase(database)) {
            return oracleGeneratorMapper;
        } else if ("sqlserver".equalsIgnoreCase(database)) {
            return sqlServerGeneratorMapper;
        } else if ("postgresql".equalsIgnoreCase(database)) {
            return postgreSQLGeneratorMapper;
        } else {
            throw new RRException("不支持当前数据库：" + database);
        }
    }

}
