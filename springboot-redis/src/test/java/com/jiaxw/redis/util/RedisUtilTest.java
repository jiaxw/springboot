package com.jiaxw.redis.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Redis 测试类
 *
 * @author jiaxw
 * @date 2020/12/1 19:19
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@SpringBootTest
public class RedisUtilTest {

    @Autowired
    public RedisUtil redisUtil;

    @Test
    public void setTest() {
        System.out.println(redisUtil.set("jiaxw", "1"));
        System.out.println(redisUtil.set("jiash", "2"));
        System.out.println(redisUtil.set("zhangzh", "3"));
        System.out.println(redisUtil.set("jxw", "4", 10));
    }

}
