/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : localhost:3306
 Source Schema         : tools

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 03/11/2020 14:37:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for country_mobile_prefix
-- ----------------------------
DROP TABLE IF EXISTS `country_mobile_prefix`;
CREATE TABLE `country_mobile_prefix`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `country` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '国家名称',
  `mobile_prefix` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '区号',
  `area` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '所在的洲',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 214 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '国际电话号码区号' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of country_mobile_prefix
-- ----------------------------
INSERT INTO `country_mobile_prefix` VALUES (1, '中国', '86', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (2, '香港', '852', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (3, '澳门', '853', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (4, '台湾', '886', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (5, '马来西亚', '60', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (6, '印度尼西亚', '62', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (7, '菲律宾', '63', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (8, '新加坡', '65', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (9, '泰国', '66', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (10, '日本', '81', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (11, '韩国', '82', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (12, '塔吉克斯坦', '7', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (13, '哈萨克斯坦', '7', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (14, '越南', '84', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (15, '土耳其', '90', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (16, '印度', '91', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (17, '巴基斯坦', '92', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (18, '阿富汗', '93', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (19, '斯里兰卡', '94', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (20, '缅甸', '95', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (21, '伊朗', '98', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (22, '亚美尼亚', '374', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (23, '东帝汶', '670', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (24, '文莱', '673', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (25, '朝鲜', '850', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (26, '柬埔寨', '855', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (27, '老挝', '856', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (28, '孟加拉国', '880', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (29, '马尔代夫', '960', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (30, '黎巴嫩', '961', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (31, '约旦', '962', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (32, '叙利亚', '963', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (33, '伊拉克', '964', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (34, '科威特', '965', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (35, '沙特阿拉伯', '966', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (36, '也门', '967', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (37, '阿曼', '968', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (38, '巴勒斯坦', '970', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (39, '阿联酋', '971', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (40, '以色列', '972', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (41, '巴林', '973', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (42, '卡塔尔', '974', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (43, '不丹', '975', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (44, '蒙古', '976', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (45, '尼泊尔', '977', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (46, '土库曼斯坦', '993', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (47, '阿塞拜疆', '994', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (48, '乔治亚', '995', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (49, '吉尔吉斯斯坦', '996', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (50, '乌兹别克斯坦', '998', '亚洲');
INSERT INTO `country_mobile_prefix` VALUES (51, '英国', '44', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (52, '德国', '49', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (53, '意大利', '39', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (54, '法国', '33', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (55, '俄罗斯', '7', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (56, '希腊', '30', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (57, '荷兰', '31', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (58, '比利时', '32', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (59, '西班牙', '34', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (60, '匈牙利', '36', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (61, '罗马尼亚', '40', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (62, '瑞士', '41', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (63, '奥地利', '43', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (64, '丹麦', '45', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (65, '瑞典', '46', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (66, '挪威', '47', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (67, '波兰', '48', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (68, '圣马力诺', '223', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (69, '匈牙利', '336', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (70, '南斯拉夫', '338', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (71, '直布罗陀', '350', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (72, '葡萄牙', '351', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (73, '卢森堡', '352', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (74, '爱尔兰', '353', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (75, '冰岛', '354', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (76, '阿尔巴尼亚', '355', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (77, '马耳他', '356', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (78, '塞浦路斯', '357', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (79, '芬兰', '358', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (80, '保加利亚', '359', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (81, '立陶宛', '370', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (82, '拉脱维亚', '371', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (83, '爱沙尼亚', '372', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (84, '摩尔多瓦', '373', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (85, '安道尔共和国', '376', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (86, '乌克兰', '380', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (87, '南斯拉夫', '381', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (88, '克罗地亚', '385', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (89, '斯洛文尼亚', '386', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (90, '波黑', '387', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (91, '马其顿', '389', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (92, '梵蒂冈', '396', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (93, '捷克', '420', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (94, '斯洛伐克', '421', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (95, '列支敦士登', '423', '欧洲');
INSERT INTO `country_mobile_prefix` VALUES (96, '秘鲁', '51', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (97, '墨西哥', '52', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (98, '古巴', '53', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (99, '阿根廷', '54', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (100, '巴西', '55', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (101, '智利', '56', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (102, '哥伦比亚', '57', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (103, '委内瑞拉', '58', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (104, '福克兰群岛', '500', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (105, '伯利兹', '501', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (106, '危地马拉', '502', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (107, '萨尔瓦多', '503', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (108, '洪都拉斯', '504', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (109, '尼加拉瓜', '505', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (110, '哥斯达黎加', '506', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (111, '巴拿马', '507', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (112, '圣彼埃尔', '508', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (113, '海地', '509', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (114, '瓜德罗普', '590', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (115, '玻利维亚', '591', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (116, '圭亚那', '592', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (117, '厄瓜多尔', '593', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (118, '法属圭亚那', '594', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (119, '巴拉圭', '595', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (120, '马提尼克', '596', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (121, '苏里南', '597', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (122, '乌拉圭', '598', '南美洲');
INSERT INTO `country_mobile_prefix` VALUES (123, '埃及', '20', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (124, '南非', '27', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (125, '摩洛哥', '212', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (126, '阿尔及利亚', '213', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (127, '突尼斯', '216', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (128, '利比亚', '218', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (129, '冈比亚', '220', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (130, '塞内加尔', '221', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (131, '毛里塔尼亚', '222', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (132, '马里', '223', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (133, '几内亚', '224', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (134, '科特迪瓦', '225', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (135, '布基拉法索', '226', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (136, '尼日尔', '227', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (137, '多哥', '228', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (138, '贝宁', '229', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (139, '毛里求斯', '230', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (140, '利比里亚', '231', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (141, '塞拉利昂', '232', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (142, '加纳', '233', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (143, '尼日利亚', '234', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (144, '乍得', '235', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (145, '中非', '236', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (146, '喀麦隆', '237', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (147, '佛得角', '238', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (148, '圣多美', '239', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (149, '普林西比', '239', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (150, '赤道几内亚', '240', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (151, '加蓬', '241', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (152, '刚果', '242', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (153, '扎伊尔', '243', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (154, '安哥拉', '244', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (155, '几内亚比绍', '245', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (156, '阿森松', '247', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (157, '塞舌尔', '248', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (158, '苏丹', '249', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (159, '卢旺达', '250', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (160, '埃塞俄比亚', '251', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (161, '索马里', '252', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (162, '吉布提', '253', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (163, '肯尼亚', '254', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (164, '坦桑尼亚', '255', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (165, '乌干达', '256', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (166, '布隆迪', '257', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (167, '莫桑比克', '258', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (168, '赞比亚', '260', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (169, '马达加斯加', '261', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (170, '留尼旺岛', '262', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (171, '津巴布韦', '263', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (172, '纳米比亚', '264', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (173, '马拉维', '265', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (174, '莱索托', '266', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (175, '博茨瓦纳', '267', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (176, '斯威士兰', '268', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (177, '科摩罗', '269', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (178, '圣赫勒拿', '290', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (179, '厄立特里亚', '291', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (180, '阿鲁巴岛', '297', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (181, '法罗群岛', '298', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (182, '摩纳哥', '377', '非洲');
INSERT INTO `country_mobile_prefix` VALUES (183, '澳大利亚', '61', '大洋洲');
INSERT INTO `country_mobile_prefix` VALUES (184, '新西兰', '64', '大洋洲');
INSERT INTO `country_mobile_prefix` VALUES (185, '关岛', '671', '大洋洲');
INSERT INTO `country_mobile_prefix` VALUES (186, '瑙鲁', '674', '大洋洲');
INSERT INTO `country_mobile_prefix` VALUES (187, '汤加', '676', '大洋洲');
INSERT INTO `country_mobile_prefix` VALUES (188, '所罗门群岛', '677', '大洋洲');
INSERT INTO `country_mobile_prefix` VALUES (189, '瓦努阿图', '678', '大洋洲');
INSERT INTO `country_mobile_prefix` VALUES (190, '斐济', '679', '大洋洲');
INSERT INTO `country_mobile_prefix` VALUES (191, '科克群岛', '682', '大洋洲');
INSERT INTO `country_mobile_prefix` VALUES (192, '纽埃岛', '683', '大洋洲');
INSERT INTO `country_mobile_prefix` VALUES (193, '东萨摩亚', '684', '大洋洲');
INSERT INTO `country_mobile_prefix` VALUES (194, '西萨摩亚', '685', '大洋洲');
INSERT INTO `country_mobile_prefix` VALUES (195, '基里巴斯', '686', '大洋洲');
INSERT INTO `country_mobile_prefix` VALUES (196, '图瓦卢', '688', '大洋洲');
INSERT INTO `country_mobile_prefix` VALUES (197, '科科斯岛', '619162', '大洋洲');
INSERT INTO `country_mobile_prefix` VALUES (198, '诺福克岛', '6723', '大洋洲');
INSERT INTO `country_mobile_prefix` VALUES (199, '圣诞岛', '619164', '大洋洲');
INSERT INTO `country_mobile_prefix` VALUES (200, '美国', '1', '北美洲');
INSERT INTO `country_mobile_prefix` VALUES (201, '加拿大', '1', '北美洲');
INSERT INTO `country_mobile_prefix` VALUES (202, '夏威夷', '1808', '北美洲');
INSERT INTO `country_mobile_prefix` VALUES (203, '阿拉斯加', '1907', '北美洲');
INSERT INTO `country_mobile_prefix` VALUES (204, '格陵兰岛', '299', '北美洲');
INSERT INTO `country_mobile_prefix` VALUES (205, '中途岛', '1808', '北美洲');
INSERT INTO `country_mobile_prefix` VALUES (206, '威克岛', '1808', '北美洲');
INSERT INTO `country_mobile_prefix` VALUES (207, '维尔京群岛', '1809', '北美洲');
INSERT INTO `country_mobile_prefix` VALUES (208, '波多黎各', '1809', '北美洲');
INSERT INTO `country_mobile_prefix` VALUES (209, '巴哈马', '1809', '北美洲');
INSERT INTO `country_mobile_prefix` VALUES (210, '安圭拉岛', '1809', '北美洲');
INSERT INTO `country_mobile_prefix` VALUES (211, '圣卢西亚', '1809', '北美洲');
INSERT INTO `country_mobile_prefix` VALUES (212, '巴巴多斯', '1809', '北美洲');
INSERT INTO `country_mobile_prefix` VALUES (213, '牙买加', '1876', '北美洲');
INSERT INTO `country_mobile_prefix` VALUES (214, '南极洲', '64672', '南极洲');

-- ----------------------------
-- Table structure for cron
-- ----------------------------
DROP TABLE IF EXISTS `cron`;
CREATE TABLE `cron`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '任务名称',
  `cron` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '任务表达式',
  `tag` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '表达式标签',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cron
-- ----------------------------
INSERT INTO `cron` VALUES (1, 'one_seconds', '*/5 * * * * ?', '每隔5秒执行一次');
INSERT INTO `cron` VALUES (2, 'one_seconds', '*/1 * * * * ?', '每秒钟一次');
INSERT INTO `cron` VALUES (3, 'one_seconds', '*/5 * * * * ?', '每隔5秒执行一次');
INSERT INTO `cron` VALUES (4, 'one_seconds', '0 */1 * * * ?', '每隔1分钟执行一次');
INSERT INTO `cron` VALUES (5, 'one_seconds', '0 0 23 * * ?', '每天23点执行一次');
INSERT INTO `cron` VALUES (6, 'one_seconds', '0 0 1 * * ?', '每天凌晨1点执行一次');
INSERT INTO `cron` VALUES (7, 'one_seconds', '0 0 1 1 * ?', '每月1号凌晨1点执行一次');
INSERT INTO `cron` VALUES (8, 'one_seconds', '0 0 23 L * ?', '每月最后一天23点执行一次');
INSERT INTO `cron` VALUES (9, 'one_seconds', '0 0 1 ? * L', '每周星期天凌晨1点实行一次');
INSERT INTO `cron` VALUES (10, 'one_seconds', '0 26,29,33 * * * ?', '在26分、29分、33分执行一次');
INSERT INTO `cron` VALUES (11, 'one_seconds', '0 0 0,13,18,21 * * ?', '每天的0点、13点、18点、21点都执行一次');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `login_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '登录名',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码',
  `sex` tinyint UNSIGNED NOT NULL DEFAULT 10 COMMENT '性别10:男;11:女;12:其他',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '头像地址',
  `status` tinyint UNSIGNED NOT NULL DEFAULT 10 COMMENT '账号状态10:正常;20:锁定;30:注销',
  `del_flag` tinyint UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除标识0(true):未删除;1(false):已删除',
  `create_by` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
