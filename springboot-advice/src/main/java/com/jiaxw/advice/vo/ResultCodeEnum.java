package com.jiaxw.advice.vo;

import com.jiaxw.common.exception.BaseErrorInfoInterface;
import io.swagger.annotations.ApiModelProperty;

/**
 * 异常信息枚举类，添加错误信息要加文档注释
 *
 * @author jiaxw
 * @date 2020/12/2 11:24
 */
public enum ResultCodeEnum implements BaseErrorInfoInterface {

    /**
     * 成功!
     */
    SUCCESS("200", "成功!"),

    /**
     * 请求的数据格式不符!
     */
    BODY_NOT_MATCH("400", "请求的数据格式不符!"),

    /**
     * 请求的数字签名不匹配!
     */
    SIGNATURE_NOT_MATCH("401", "请求的数字签名不匹配!"),

    /**
     * 未找到该资源!
     */
    NOT_FOUND("404", "未找到该资源!"),

    /**
     * 服务器内部错误!
     */
    INTERNAL_SERVER_ERROR("500", "服务器内部错误!"),

    /**
     * 服务器正忙，请稍后再试!
     */
    SERVER_BUSY("503", "服务器正忙，请稍后再试!");


    // 下面的代码不能放在前面，会出错

    /**
     * 错误码
     */
    @ApiModelProperty(value = "返回码")
    private String resultCode;

    /**
     * 错误描述
     */
    @ApiModelProperty(value = "返回消息")
    private String resultMsg;

    ResultCodeEnum(String resultCode, String resultMsg) {
        this.resultCode = resultCode;
        this.resultMsg = resultMsg;
    }

    @Override
    public String getResultCode() {
        return resultCode;
    }

    @Override
    public String getResultMsg() {
        return resultMsg;
    }

}
