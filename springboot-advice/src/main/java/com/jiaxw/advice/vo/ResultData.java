package com.jiaxw.advice.vo;

import com.jiaxw.common.exception.BaseErrorInfoInterface;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 统一返回json格式
 *
 * @author jiaxw
 * @date 2020/12/1 14:30
 */
@ApiModel(value = "统一返回json格式", description = "统一返回json格式")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultData implements Serializable {

    /**
     * 是否成功
     */
    @ApiModelProperty(value = "是否成功")
    private Boolean success;

    /**
     * 返回码
     */
    @ApiModelProperty(value = "返回码")
    private String code;

    /**
     * 返回消息
     */
    @ApiModelProperty(value = "返回消息")
    private String message;

    /**
     * 返回数据
     */
    @ApiModelProperty(value = "返回数据")
    private Map<String, Object> data = new HashMap<>();

    /**
     * 成功静态方法
     *
     * @return
     */
    public static ResultData ok() {
        ResultData result = new ResultData();
        result.setSuccess(true);
        result.setCode(ResultCodeEnum.SUCCESS.getResultCode());
        result.setMessage("成功!");
        return result;
    }

    /**
     * 失败静态方法
     *
     * @return
     */
    public static ResultData error() {
        ResultData result = new ResultData();
        result.setSuccess(false);
        result.setCode(ResultCodeEnum.SERVER_BUSY.getResultCode());
        result.setMessage("失败!");
        return result;
    }

    /**
     * 失败
     */
    public static ResultData error(BaseErrorInfoInterface errorInfo) {
        ResultData result = new ResultData();
        result.setSuccess(false);
        result.setCode(errorInfo.getResultCode());
        result.setMessage(errorInfo.getResultMsg());
        return result;
    }

    /**
     * 失败
     */
    public static ResultData error(String code, String message) {
        ResultData result = new ResultData();
        result.setSuccess(false);
        result.setCode(code);
        result.setMessage(message);
        return result;
    }

    public ResultData success(Boolean success) {
        this.setSuccess(success);
        return this;
    }

    public ResultData code(String code) {
        this.setCode(code);
        return this;
    }

    public ResultData message(String message) {
        this.setMessage(message);
        return this;
    }

    public ResultData data(String key, Object value) {
        this.data.put(key, value);
        return this;
    }

    public ResultData data(Map<String, Object> map) {
        this.setData(map);
        return this;
    }

}
