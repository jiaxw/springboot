package com.jiaxw.cron.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 定时任务
 *
 * @author jiaxw
 * @date 2020/12/5 12:19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "cron")
public class CronEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id")
    private Long id;

    /**
     * 任务名称
     */
    private String name;

    /**
     * 任务表达式
     */
    private String cron;

    /**
     * 表达式描述
     */
    private String tag;

}
