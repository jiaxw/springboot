package com.jiaxw.cron.task;

import com.jiaxw.cron.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * 简单的定时任务
 *
 * @author jiaxw
 * @date 2020/12/5 12:19
 */
@Slf4j
@Configuration
public class ScheduleTask {

    @Scheduled(cron = "0/5 * * * * ?")
    private void configureTasks() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("[configureTasks] cron: " + DateUtil.getCurrentDateTime());
    }

    /**
     * 固定效率执行
     */
    @Scheduled(fixedRate = 5000)
    private void fixedRateTask() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("[fixedRateTask] 固定效率执行: " + DateUtil.getCurrentDateTime());
    }

    /**
     * 固定延迟执行
     */
    @Scheduled(fixedDelay = 5000)
    private void fixedDelayTask() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("[fixedDelayTask] 固定延迟执行: " + DateUtil.getCurrentDateTime());
    }

}
