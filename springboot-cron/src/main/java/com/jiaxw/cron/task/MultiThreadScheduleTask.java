package com.jiaxw.cron.task;

import com.jiaxw.cron.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * 测试固定延迟定时任务，采用多线程可以解决多个任务之间的干扰
 *
 * @author jiaxw
 * @date 2020/12/5 12:19
 */
@Slf4j
@Configuration
public class MultiThreadScheduleTask {

    @Async
    @Scheduled(fixedDelay = 1000)
    public void first() throws InterruptedException {
        log.info("第一个定时任务开始 : " + DateUtil.getCurrentDateTime() + "\r\n线程 : " + Thread.currentThread().getName());
        Thread.sleep(1000 * 10);
    }

    @Async
    @Scheduled(fixedDelay = 2000)
    public void second() {
        log.info("第二个定时任务开始 : " + DateUtil.getCurrentDateTime() + "\r\n线程 : " + Thread.currentThread().getName());
    }

}
