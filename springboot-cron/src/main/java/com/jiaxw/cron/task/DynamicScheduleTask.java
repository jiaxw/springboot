package com.jiaxw.cron.task;

import com.jiaxw.cron.entity.CronEntity;
import com.jiaxw.cron.service.CronService;
import com.jiaxw.cron.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.util.StringUtils;

/**
 * 动态定时任务
 *
 * @author jiaxw
 * @date 2020/12/5 12:19
 */
@Slf4j
@Configuration
public class DynamicScheduleTask implements SchedulingConfigurer {

    /**
     * 默认一秒钟一次
     */
    private static final String DEFAULT_CRON = "*/1 * * * * ?";

    @Autowired
    private CronService cronService;

    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        scheduledTaskRegistrar.addTriggerTask(
                () -> log.info("执行动态定时任务: " + DateUtil.getCurrentDateTime()),
                triggerContext -> {
                    // 查询数据库中的任务调度
                    CronEntity entity = cronService.getById(1);
                    if (StringUtils.isEmpty(entity.getCron())) {
                        // 表达式错误
                        log.error("表达式错误" + DateUtil.getCurrentDateTime());
                        // 给一个默认的表达式
                        return new CronTrigger(DEFAULT_CRON).nextExecutionTime(triggerContext);
                    }
                    // 需要做的业务
                    log.info("成功执行动态定时任务" + DateUtil.getCurrentDateTime());
                    // 下次执行任务
                    return new CronTrigger(entity.getCron()).nextExecutionTime(triggerContext);
                }
        );
    }

}
