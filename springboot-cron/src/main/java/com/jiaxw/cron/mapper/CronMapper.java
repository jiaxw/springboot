package com.jiaxw.cron.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jiaxw.cron.entity.CronEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 定时任务mapper
 *
 * @author jiaxw
 * @date 2020/12/5 12:19
 */
@Mapper
public interface CronMapper extends BaseMapper<CronEntity> {

}
