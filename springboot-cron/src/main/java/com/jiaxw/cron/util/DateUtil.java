package com.jiaxw.cron.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author jiaxw
 * @date 2020/12/5 12:32
 */
public class DateUtil {

    /**
     * 显示年月日时分秒
     */
    public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 获取当前时间
     *
     * @return
     */
    public static String getCurrentDateTime() {
        SimpleDateFormat df = new SimpleDateFormat(DATETIME_FORMAT);
        Date date = new Date();
        return df.format(date);
    }
}