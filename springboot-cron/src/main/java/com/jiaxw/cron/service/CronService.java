package com.jiaxw.cron.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jiaxw.cron.entity.CronEntity;

/**
 * 定时任务
 *
 * @author jiaxw
 * @date 2020/12/5 12:19
 */
public interface CronService extends IService<CronEntity> {

}
