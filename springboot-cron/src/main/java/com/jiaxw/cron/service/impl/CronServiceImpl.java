package com.jiaxw.cron.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jiaxw.cron.entity.CronEntity;
import com.jiaxw.cron.mapper.CronMapper;
import com.jiaxw.cron.service.CronService;
import org.springframework.stereotype.Service;

/**
 * 定时任务
 *
 * @author jiaxw
 * @date 2020/12/5 12:19
 */
@Service("cronService")
public class CronServiceImpl extends ServiceImpl<CronMapper, CronEntity> implements CronService {

}
