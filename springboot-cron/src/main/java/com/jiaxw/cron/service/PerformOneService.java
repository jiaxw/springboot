package com.jiaxw.cron.service;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

/**
 * 该方法只会执行一次
 *
 * @author jiaxw
 * @date 2020/12/5 12:19
 */
@Service
public class PerformOneService implements ApplicationListener<ContextRefreshedEvent> {

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        // 保证只执行一次
        if (null == contextRefreshedEvent.getApplicationContext().getParent()) {
            // 需要执行的方法
            System.out.println("该方法只会执行一次");
        }
    }

}
