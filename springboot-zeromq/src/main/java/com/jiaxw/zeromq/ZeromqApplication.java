package com.jiaxw.zeromq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jiaxw
 * @date 2020/12/8 14:30
 */
@SpringBootApplication
public class ZeromqApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZeromqApplication.class, args);
    }

}
