package com.jiaxw.zeromq.provider.rep;

import org.zeromq.SocketType;
import org.zeromq.ZMQ;

/**
 * 请求/响应模式
 *
 * @author jiaxw
 * @date 2020/12/8 14:30
 */
public class Response {

    public static void main(String[] args) {
        // I/O线程上下文的数量为1
        ZMQ.Context context = ZMQ.context(1);
        // ZMQ.REP表示这是一个reponse类型的socket
        ZMQ.Socket socket = context.socket(SocketType.REP);
        // 绑定到8888端口
        socket.bind("tcp://*:8888");
        while (true) {
            byte[] request = socket.recv();
            if (new String(request).equals("END")) {
                break;
            }
            System.out.println("Response recv:\t" + new String(request));
            String response = "I got it";
            socket.send(response.getBytes());
        }
        //关闭
        socket.close();
        context.term();
    }

}
