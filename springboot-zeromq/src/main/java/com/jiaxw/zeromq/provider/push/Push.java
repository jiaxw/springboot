package com.jiaxw.zeromq.provider.push;

import org.zeromq.SocketType;
import org.zeromq.ZMQ;

/**
 * @author jiaxw
 * @date 2020/12/8 14:30
 */
public class Push {

    public static void main(String[] args) {
        ZMQ.Context context = ZMQ.context(1);
        ZMQ.Socket push = context.socket(SocketType.PUSH);
        push.bind("ipc://fjs");
        for (int i = 0; i < 10000000; i++) {
            push.send("hello".getBytes());
        }
        push.close();
        context.term();
    }

}
