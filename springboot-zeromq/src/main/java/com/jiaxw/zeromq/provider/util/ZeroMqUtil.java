package com.jiaxw.zeromq.provider.util;

import org.springframework.context.annotation.Configuration;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

/**
 * ZeroMq工具类
 *
 * @author jiaxw
 * @date 2020/12/8 14:30
 */
@Configuration
public class ZeroMqUtil {

    /**
     * 发送 ZeroMq 消息
     *
     * @param msg        消息体
     * @param endPoint   绑定IP端口
     * @param socketType 消息类型
     */
    public void send(String msg, String endPoint, SocketType socketType) {
        ZContext context = new ZContext();
        ZMQ.Socket socket = context.createSocket(socketType);
        socket.bind(endPoint);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        socket.send(msg);
        socket.close();
        context.destroy();
    }

}
