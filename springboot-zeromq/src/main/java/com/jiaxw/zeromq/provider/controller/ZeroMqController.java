package com.jiaxw.zeromq.provider.controller;

import com.jiaxw.common.vo.ResultData;
import com.jiaxw.zeromq.provider.util.ZeroMqUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zeromq.SocketType;

/**
 * 测试ZeroMq的Controller
 *
 * @author jiaxw
 * @date 2020/12/8 14:30
 */
@Api(tags = "ZeroMQ 测试类")
@RestController
public class ZeroMqController {

    @Autowired
    private ZeroMqUtil zeroMqUtil;

    @ApiOperation(value = "testPub")
    @GetMapping("testPub")
    public ResultData testPub() {
        zeroMqUtil.send("Time:testPub", "tcp://*:5555", SocketType.PUB);
        return ResultData.ok();
    }

    @ApiOperation(value = "testPush")
    @GetMapping("testPush")
    public ResultData testPush() {
        zeroMqUtil.send("Time:testPush", "ipc://fjs", SocketType.PUSH);
        return ResultData.ok();
    }

}
