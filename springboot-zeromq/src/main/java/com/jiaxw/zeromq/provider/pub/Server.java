package com.jiaxw.zeromq.provider.pub;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import java.util.Random;

/**
 * 发布/订阅模式
 *
 * @author jiaxw
 * @date 2020/12/8 14:30
 */
public class Server {

    public static void main(String[] args) {
        try (ZContext context = new ZContext()) {
            // publish类型
            ZMQ.Socket publisher = context.createSocket(SocketType.PUB);
            publisher.bind("tcp://*:5555");

            Random random = new Random();
            while (true) {
                String update;
                // 随机将update赋值为Time: 或Order: 开头的值
                if (random.nextInt(10) <= 5) {
                    update = "Time: " + System.currentTimeMillis();
                } else {
                    update = "Order: " + System.currentTimeMillis();
                }
                // 发送
                publisher.send(update);
                System.out.println("SEND:[" + update + "]");
            }
        }
    }

}
