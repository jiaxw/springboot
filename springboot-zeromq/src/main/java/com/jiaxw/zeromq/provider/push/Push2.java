package com.jiaxw.zeromq.provider.push;

import org.zeromq.SocketType;
import org.zeromq.ZMQ;

/**
 * push端来连接pull端
 *
 * @author jiaxw
 * @date 2020/12/8 14:30
 */
public class Push2 {

    public static void main(String[] args) {
        for (int j = 0; j < 3; j++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    ZMQ.Context context = ZMQ.context(1);
                    ZMQ.Socket push = context.socket(SocketType.PUSH);
                    push.connect("ipc://fjs");
                    for (int i = 0; i < 10000000; i++) {
                        push.send("hello".getBytes());
                        System.out.println(i);
                    }
                    push.close();
                    context.term();
                }
            }).start();
        }
    }

}
