package com.jiaxw.zeromq.consumer.sub;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

/**
 * 发布/订阅模式
 *
 * @author jiaxw
 * @date 2020/12/8 14:30
 */
public class Client {

    public static void main(String[] args) {
        try (ZContext context = new ZContext()) {
            // subscribe类型
            ZMQ.Socket subscriber = context.createSocket(SocketType.SUB);
            subscriber.connect("tcp://localhost:5555");

            // 只订阅Time: 开头的信息
            subscriber.subscribe("Time:".getBytes());

            for (int i = 0; i < 10; i++) {
                // recvStr直接返回String，内部调用了recv，将byte数组转化为String
                System.out.println(subscriber.recvStr());
            }
        }
    }

}
