package com.jiaxw.zeromq.consumer.pull;

import org.zeromq.SocketType;
import org.zeromq.ZMQ;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author jiaxw
 * @date 2020/12/8 14:30
 */
public class Pull {

    public static void main(String[] args) {
        final AtomicInteger number = new AtomicInteger(0);
        for (int i = 0; i < 5; i++) {
            new Thread(new Runnable() {

                private int here = 0;

                @Override
                public void run() {
                    ZMQ.Context context = ZMQ.context(1);
                    ZMQ.Socket pull = context.socket(SocketType.PULL);
                    pull.connect("ipc://fjs");
                    while (true) {
                        String message = new String(pull.recv());
                        int now = number.incrementAndGet();
                        here++;
                        if (now % 1000000 == 0) {
                            System.out.println(now + "  here is : " + here);
                        }
                    }
                }
            }).start();
        }
    }

}

