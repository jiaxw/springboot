package com.jiaxw.zeromq.consumer.req;

import org.zeromq.SocketType;
import org.zeromq.ZMQ;

/**
 * 请求/响应模式
 *
 * @author jiaxw
 * @date 2020/12/8 14:30
 */
public class Request {

    public static void main(String[] args) {
        // I/O线程上下文的数量为1
        ZMQ.Context context = ZMQ.context(1);
        // ZMQ.REQ表示这是一个request类型的socket
        ZMQ.Socket socket = context.socket(SocketType.REQ);
        // 连接到8888端口
        socket.connect("tcp://127.0.0.1:8888");
        for (int i = 0; i < 10; i++) {
            long now = System.currentTimeMillis();
            String request = "hello, time is " + now;
            socket.send(request.getBytes());
            byte[] response = socket.recv();
            System.out.println("Request recv:\t" + new String(response));
        }
        socket.send("END".getBytes());

        // 关闭
        socket.close();
        context.term();
    }

}
