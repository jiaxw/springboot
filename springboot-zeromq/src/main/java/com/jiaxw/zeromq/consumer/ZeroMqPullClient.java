package com.jiaxw.zeromq.consumer;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

/**
 * @author jiaxw
 * @date 2020/12/8 14:30
 */
public class ZeroMqPullClient {

    /**
     * 通信SOCKET套接字
     */
    private ZMQ.Socket socket;

    /**
     * 订阅端点
     */
    private String endPoint;

    public ZeroMqPullClient(String endPoint) {
        this.endPoint = endPoint;
        build();
    }

    private void build() {
        ZContext context = new ZContext();
        socket = context.createSocket(SocketType.PULL);
        // 指定监控所有的事件
        socket.monitor("inproc://reqmoniter", ZMQ.EVENT_ALL);
        // 启动事件监控线程
        new Thread(new MoniterMqTask(context)).start();
        socket.connect(endPoint);
    }

    /**
     * 获取服务端SOCKET对象
     *
     * @return
     */
    public ZMQ.Socket getSocket() {
        return this.socket;
    }

}
