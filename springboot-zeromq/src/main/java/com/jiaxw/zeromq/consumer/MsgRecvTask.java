package com.jiaxw.zeromq.consumer;

import org.zeromq.ZMQ;

/**
 * ZEROMQ接收订阅TOPIC通道消息
 *
 * @author jiaxw
 * @date 2020/12/8 14:30
 */
public class MsgRecvTask implements Runnable {

    /**
     * SOCKET对象
     */
    private ZMQ.Socket socket;

    public MsgRecvTask(ZMQ.Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        int count = 0;
        while (true) {
            count++;
            System.out.println(count + ":接收到数据：" + socket.recvStr());
        }
    }

}
