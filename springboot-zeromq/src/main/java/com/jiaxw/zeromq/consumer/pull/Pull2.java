package com.jiaxw.zeromq.consumer.pull;

import org.zeromq.SocketType;
import org.zeromq.ZMQ;

/**
 * pull端建立监听
 *
 * @author jiaxw
 * @date 2020/12/8 14:30
 */
public class Pull2 {

    public static void main(String[] args) {
        ZMQ.Context context = ZMQ.context(1);
        ZMQ.Socket pull = context.socket(SocketType.PULL);
        pull.bind("ipc://fjs");
        int number = 0;
        while (true) {
            String message = new String(pull.recv());
            number++;
            if (number % 10000 == 0) {
                System.out.println(message + "" + number);
            }
        }
    }

}

